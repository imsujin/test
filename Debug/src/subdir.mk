################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/LinuxCAN.cpp \
../src/LinuxEther.cpp \
../src/PTPHardwareClock.cpp \
../src/UDPPacketParser.cpp \
../src/main.cpp \

CPP_SRCS_QUOTED += \
"../src/LinuxCAN.cpp" \
"../src/LinuxEther.cpp" \
"../src/PTPHardwareClock.cpp" \
"../src/UDPPacketParser.cpp" \
"../src/main.cpp" \

CPP_DEPS_QUOTED += \
"./src/LinuxCAN.d" \
"./src/LinuxEther.d" \
"./src/PTPHardwareClock.d" \
"./src/UDPPacketParser.d" \
"./src/main.d" \

OBJS_QUOTED += \
"./src/LinuxCAN.o" \
"./src/LinuxEther.o" \
"./src/PTPHardwareClock.o" \
"./src/UDPPacketParser.o" \
"./src/main.o" \

OBJS_OS_FORMAT += \
./src/LinuxCAN.o \
./src/LinuxEther.o \
./src/PTPHardwareClock.o \
./src/UDPPacketParser.o \
./src/main.o \

OBJS += \
./src/LinuxCAN.o \
./src/LinuxEther.o \
./src/PTPHardwareClock.o \
./src/UDPPacketParser.o \
./src/main.o \

CPP_DEPS += \
./src/LinuxCAN.d \
./src/LinuxEther.d \
./src/PTPHardwareClock.d \
./src/UDPPacketParser.d \
./src/main.d \


# Each subdirectory must supply rules for building sources it contributes
src/LinuxCAN.o: ../src/LinuxCAN.cpp
	@echo 'Building file: $<'
	@echo 'Executing target #1 $<'
	@echo 'Invoking: Standard S32DS C++ Compiler'
	aarch64-linux-gnu-g++ "@src/LinuxCAN.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "src/LinuxCAN.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/LinuxEther.o: ../src/LinuxEther.cpp
	@echo 'Building file: $<'
	@echo 'Executing target #2 $<'
	@echo 'Invoking: Standard S32DS C++ Compiler'
	aarch64-linux-gnu-g++ "@src/LinuxEther.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "src/LinuxEther.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/PTPHardwareClock.o: ../src/PTPHardwareClock.cpp
	@echo 'Building file: $<'
	@echo 'Executing target #3 $<'
	@echo 'Invoking: Standard S32DS C++ Compiler'
	aarch64-linux-gnu-g++ "@src/PTPHardwareClock.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "src/PTPHardwareClock.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/UDPPacketParser.o: ../src/UDPPacketParser.cpp
	@echo 'Building file: $<'
	@echo 'Executing target #4 $<'
	@echo 'Invoking: Standard S32DS C++ Compiler'
	aarch64-linux-gnu-g++ "@src/UDPPacketParser.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "src/UDPPacketParser.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/main.o: ../src/main.cpp
	@echo 'Building file: $<'
	@echo 'Executing target #5 $<'
	@echo 'Invoking: Standard S32DS C++ Compiler'
	aarch64-linux-gnu-g++ "@src/main.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "src/main.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


