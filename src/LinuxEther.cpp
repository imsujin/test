/*
 * UDP.cpp
 *
 *  Created on: 2020. 03. 02.
 *      Author: Mario _KATECH
 */
 
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "LinuxEther.h"
#include "Frametest.h"

#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include "ether.h"

#define V2XTIMRXPORT 7002
#define V2XSPaTRXPORT 7001
#define MAPRXPORT 2000
#define GPSRXPORT 3003
#define GPSTimeRXPORT 3001
#define LPMRXPORT 4000
#define PVDTXPORT 5000
#define PVDACTXPORT 5001
//#define CANOERXPORT 2001
#define CANOERXPORT 2368
//#define CANOERXPORT1 2002

#define BUF_SIZ		1024
#define ETH_AVTP_STREAM_LEN	20
#define ETH_AVTP_CONTROL_LEN	12
#define ETH_PAYLOAD_STREAM_LEN	990	//1024-14-20
#define ETH_PAYLOAD_CONTROL_LEN	998	//1024-14-12

Test_Frame test;

//Extern variables for LinuxEther.cpp
extern uint8_t GearPosition;
extern uint8_t AcceleratorAct;
extern uint32_t CurrentVehicleLatitude;
extern uint32_t CurrentVehicleLongitude;
extern float VS;
extern uint8_t HeadLightStatus;
extern uint8_t RightTurnStatus;
extern uint8_t LeftTurnStatus;
extern uint8_t ACCAct;
extern float AccelPedPos;
extern float Fb_Brake_Press1;
extern float CurrentSteeringAngle;
extern float StrTorque;
extern float LongAccel;
extern float LatAccel;
extern float YawRate;
//Extern variables for LinuxEther.cpp

uint8_t AliveLiDAR_FL = 0;
uint8_t AliveLiDAR_FR = 0;
uint8_t AliveLiDAR_VFOV_F = 0;
uint8_t AliveLiDAR_VFOV_L = 0;
uint8_t AliveLiDAR_VFOV_R = 0;
uint8_t AliveLiDAR_RL = 0;
uint8_t AliveLiDAR_RR = 0;
uint8_t AliveLPM = 0;
uint8_t AliveGPSTime = 0;
uint8_t AliveGPS = 0;
uint8_t AliveV2XMAP = 0;
uint8_t AliveV2XSPaT = 0;
uint8_t AliveV2XTIM = 0;
uint8_t AliveESU1 = 0;
uint8_t AliveESU2 = 0;

uint8_t TIMRequestVelocity = 0;

uint32_t RawVehicleLatitude = 0;
uint32_t RawVehicleLongitude = 0;
uint32_t RawVehicleAltidude = 0;
uint32_t RawVehicleNorthVelocity = 0;
uint32_t RawVehicleEastVelocity = 0;
uint32_t RawVehicleDownVelocity = 0;
uint32_t RawVehicleRoll = 0;
uint32_t RawVehiclePitch = 0;
uint32_t RawVehicleHeading = 0;
uint8_t RawVehicleHDLReadType = 0;
uint16_t RawVehicleLongitudinalHDL = 0;
uint16_t RawVehicleLateralHDL = 0;
uint8_t RawVehicleHDLWarning = 0;

float VehicleLatitude = 0;
float VehicleLongitude = 0;
float VehicleAltidude = 0;
float VehicleNorthVelocity = 0;
float VehicleEastVelocity = 0;
float VehicleDownVelocity = 0;
float VehicleRoll = 0;
float VehiclePitch = 0;
float VehicleHeading = 0;
float VehicleLongitudinalHDL = 0;
float VehicleLateralHDL = 0;
int CheckCnt = 0;

typedef struct {            //uint8_t UDP_field[167]; //총 167바이트 사용
	uint32_t GPS_UTC_Time;
	uint32_t Longitude;
	uint32_t Latitude;
	int32_t Elevation;
	uint16_t Heading;
	uint16_t Transmission; //GPS
	uint16_t Speed; //GPS
	int32_t PosAccuracy;//위치정확도
	uint32_t Event;
	uint32_t ExteriorLight; //외부라이트
	uint32_t LightBar;
	uint32_t BrakeStatus; //브레이크 여부
	int32_t SteeringAngle; // 핸들각도
	int32_t YawRate; //가속 상태
	uint32_t ThrottlePosition; //쓰로틀 위치
	uint32_t GNSSStatus; //쓰로틀 위치

	uint32_t DrivingMode; //am/mm/mrm
	uint32_t Horn; //경적아림
	uint32_t Fallback; //Fallback/MRM Alert
	uint32_t ValidationEventId;
	uint32_t ValidationEventValue;
	uint64_t ValidationEventDetail;

	uint32_t SOC;
	uint32_t H2;
	uint32_t Odometer;
	uint32_t Tripmeter;
	uint32_t distanceToEmpty;
	uint32_t TurnSignal;

	int32_t PowerGauge;

	uint32_t CurLane; //현재주행차선
	int32_t Torque; //조향토크
	int32_t Acceleration; //Long Acceleration
	uint32_t DTC; // T.B.D

	uint32_t EngineTemp;
	uint32_t CoolantTemp;
	uint32_t BatteryTemp;

	char BatteryWarning;
	char TPMSWarning;
	char ABSWarning;
	char BrakeWarning;
}PVD;
PVD PVDFormat;

typedef struct {
	char msgCnt;
	char frameType;
	uint16_t furtherInfoID;
	uint32_t roadSignLatitude;
	uint32_t roadSignLongitude;
	uint16_t headingSlice;

	uint16_t startYear;
	uint32_t startTime;
	uint16_t durationTime;
	char priority;
	uint32_t anchorLatitude;
	uint32_t anchorLongitude;
	uint16_t anchorElevation;
	uint16_t LaneWidth;
	char directionality;
	uint32_t pathLatitude;
	uint32_t pathLongitude;
	char RegionalcontentType;
	char Regionaltype;
	char RegionalValue[512];
}TIM;
TIM TIMFormat;

typedef struct {
	char name[63];
	uint16_t regionID;
	uint16_t IntersectionID;
	char revision;
	uint16_t status;
	char MovementStateCnt;
	char movementName[12][63];
	char signalGroupID[12];
	char MovementEventCnt[12];
	char eventState[12];

	uint16_t minEndTime[12];
	char ManeuverAssistCnt[12];
	char pedbicycleDetect[12];
}SPaT;
SPaT SPaTFormat;

uint8_t UDPRxBuffer[1500] = {0, };
uint8_t UDPTxBuffer[148] = {0, };
uint8_t AVTPRxBuffer[1500] = {0, };

char *iface = "pfe0";            //eth device interface name
uint16_t i = 0;
uint16_t k = 0;
unsigned char dest[ETH_ALEN]
= { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
unsigned short proto = 0x22f0;

unsigned char AVTPStreamHeader[] = //stream 20byte
{
	0x7f, //control data indicator 1(value = 0) subtype 7
	0x9c, //stream id vaild 1 , version 3 , media clock restart 1, reserved 1 , gate info valid 0, avtp timestamp valid 1 (1, 001, 1, 1, 0, 0)
	0x00, //sequence number 8
	0x00, // reserved 7, timestamp uncertain 1 (0, 0)
	0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0xa1, 0xa2, //source mac 48(8*6) unique ID 16(8*2)
	0x00, 0x00, 0x00, 0x00, //avtp timestamp 32 (depend on timestamp valid)
	0x00, 0x00, //stream data length 16 (MAX = MTU size, Payload length following the protool_specific_header field)
	0xd1, //tag 2, channel 6                       protool_specific_header field
	0xd2  //type code 4, sy 4  (for IEC 61883)     protool_specific_header field
};

unsigned char AVTPControlHeader[] = //control 12byte
{
	0x84, //control data indicator 1(value = 1) subtype 7
	0x01, //stream id vaild 1 , version 3 , contr0l data 4
	0x08, 0x5A, // control status 5, control data length 11
	0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x00, 0x01, //source mac 6 unique ID 2
};
unsigned char source[ETH_ALEN];
unsigned int AVTP_header_len = ETH_AVTP_CONTROL_LEN;
unsigned char DataPayload[ETH_PAYLOAD_STREAM_LEN] = {0, };
unsigned int DataPayloadLength = 0;
unsigned int FrameLength = 0;

int AVTPTransmit;
struct sockaddr_ll saddrll, sll;
struct ifreq ifr;
socklen_t AVTPlength = 0;

union AVTPStreamFrame
{
	struct
	{
		struct ethhdr    Header;
		unsigned char    AVTP_header[ETH_AVTP_STREAM_LEN];
		unsigned char    payload[ETH_PAYLOAD_STREAM_LEN];
	} Field;
	unsigned char    buffer[BUF_SIZ];
};

union AVTPControlFrame
{
	struct
	{
		struct ethhdr    Header;
		unsigned char    AVTPHeader[ETH_AVTP_CONTROL_LEN];
		unsigned char    Payload[ETH_PAYLOAD_CONTROL_LEN];
	} Field;
	unsigned char    Buffer[BUF_SIZ];
};

union AVTPControlFrame ControlFrame;
union AVTPStreamFrame StreamFrame;

void PVDFormatGeneration(void);


LinuxEther::LinuxEther()
{
//	rx_init();
}


LinuxEther::~LinuxEther()
{
//	pthread_cancel(udp_tx_thread);
	pthread_cancel(UDPV2XTIMRecvThread);
	pthread_cancel(UDPV2XSPaTRecvThread);
	pthread_cancel(UDPMAPRecvThread);
	pthread_cancel(UDPGPSRecvThread);
	pthread_cancel(UDPGPSTimeRecvThread);
	pthread_cancel(UDPLPMRecvThread);
	pthread_cancel(AVTPRecvThread);
//	close(tx);
	close(this->UDPRxV2XTIM);
	close(this->UDPRxV2XSPaT);
	close(this->UDPRxMAP);
	close(this->UDPRxGPS);
	close(this->UDPRxGPSTime);
	close(this->UDPRxLPM);
}
/*
int UDP::tx_init()
{
//	int sockfd_AICM,tx;
//  int enabled =1;
//	struct sockaddr_in tx_addr,AICM_addr;

	// open UDP_tx	
  if((sockfd_AICM = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
        perror("socket ");
        return 1;
    }	
	memset(&tx_addr, 0x00, sizeof(tx_addr));

	tx_addr.sin_family = AF_INET;
	tx_addr.sin_addr.s_addr = inet_addr("10.1.1.22" ); // 癰귨옙?占�?雅뚯눘??send address
// 	tx_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST); // 癰귨옙?占�?雅뚯눘??send address
	tx_addr.sin_port = htons(TXPORT);
  
	return 0;
}
*/

/*
void UDP::run_tx_thread(void)
{
//	int thr_id_tx;
	thr_id_tx = pthread_create(&this->udp_tx_thread, NULL, this->udp_tx_func, (void *) this);
}
*/

//占싹놂옙占쏙옙 占쏙옙침
int LinuxEther::UDPTxFunc(void)
{
	LinuxEther *t_tx = this;

	int send = 0;
	// open UDP_tx	
	if((t_tx->sockfd_AICM = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
        perror("socket ");
        return 1;
    }	

  	setsockopt(t_tx->sockfd_AICM, SOL_SOCKET, SO_BROADCAST, &enabled, sizeof(enabled));

 	AICM_addr.sin_family = AF_INET;
	AICM_addr.sin_addr.s_addr = inet_addr("192.168.100.255" ); //AICM 占쌍쇽옙
//	AICM_addr.sin_addr.s_addr = inet_addr("255.255.255.255" ); //AICM 占쌍쇽옙
	AICM_addr.sin_port = htons(PVDTXPORT);

	bind(t_tx->sockfd_AICM, (struct sockaddr *)&t_tx->AICM_addr, sizeof(t_tx->AICM_addr));

  	PVDFormatGeneration();
	  
  	if(sendto(t_tx->sockfd_AICM, UDPTxBuffer, sizeof(UDPTxBuffer), 0,(struct sockaddr *)&t_tx->AICM_addr, sizeof(t_tx->AICM_addr)) < 0)
	  {
		  printf("send fail %d\n");
		  exit (1);
	  }

  	CheckCnt++;
  	printf("KATECH test check ; %d\n", CheckCnt);

  	if ( CheckCnt > 100)
  		CheckCnt = 0;
//  	printf("PVDFormat.SteeringAngle ; %d\n", PVDFormat.SteeringAngle);
//  	printf("PVDFormat.Transmission ; %u\n", PVDFormat.Transmission);
//  	printf("PVDFormat.Speed ; %u\n", PVDFormat.Speed);
  	/*

 	printf("PVDFormat.GPS_UTC_Time ; %u\n", PVDFormat.GPS_UTC_Time);
  	printf("PVDFormat.Longitude ; %u\n", PVDFormat.Longitude);
  	printf("PVDFormat.Latitude ; %u\n", PVDFormat.Latitude);
  	printf("PVDFormat.Elevation ; %u\n", PVDFormat.Elevation);
  	printf("PVDFormat.Heading ; %u\n", PVDFormat.Heading);
  	printf("PVDFormat.Transmission ; %u\n", PVDFormat.Transmission);
  	printf("PVDFormat.Speed ; %u\n", PVDFormat.Speed);
  	printf("PVDFormat.PosAccuracy ; %d\n", PVDFormat.PosAccuracy);
  	printf("PVDFormat.Event ; %u\n", PVDFormat.Event);
  	printf("PVDFormat.ExteriorLight ; %u\n", PVDFormat.ExteriorLight);
  	printf("PVDFormat.LightBar ; %u\n", PVDFormat.LightBar);
  	printf("PVDFormat.BrakeStatus ; %u\n", PVDFormat.BrakeStatus);
  	printf("PVDFormat.SteeringAngle ; %d\n", PVDFormat.SteeringAngle);
  	printf("PVDFormat.YawRate ; %d\n", PVDFormat.YawRate);
  	printf("PVDFormat.ThrottlePosition ; %u\n", PVDFormat.ThrottlePosition);
  	printf("PVDFormat.GNSSStatus ; %u\n", PVDFormat.GNSSStatus);
  	printf("PVDFormat.DrivingMode ; %u\n", PVDFormat.DrivingMode);
  	printf("PVDFormat.Horn ; %u\n", PVDFormat.Horn);
  	printf("PVDFormat.Fallback ; %u\n", PVDFormat.Fallback);
  	printf("PVDFormat.ValidationEventId ; %u\n", PVDFormat.ValidationEventId);
  	printf("PVDFormat.ValidationEventValue ; %u\n", PVDFormat.ValidationEventValue);
  	printf("PVDFormat.ValidationEventDetail ; %u\n", PVDFormat.ValidationEventDetail);
  	printf("PVDFormat.SOC ; %u\n", PVDFormat.SOC);
  	printf("PVDFormat.H2 ; %u\n", PVDFormat.H2);
  	printf("PVDFormat.Odometer ; %u\n", PVDFormat.Odometer);
  	printf("PVDFormat.Tripmeter ; %u\n",PVDFormat.Tripmeter);
  	printf("PVDFormat.distanceToEmpty ; %u\n", PVDFormat.distanceToEmpty);
  	printf("PVDFormat.TurnSignal ; %u\n", PVDFormat.TurnSignal);
  	printf("PVDFormat.PowerGauge ; %d\n", PVDFormat.PowerGauge);
  	printf("PVDFormat.CurLane ; %u\n", PVDFormat.CurLane);
  	printf("PVDFormat.Torque ; %d\n", PVDFormat.Torque);
  	printf("PVDFormat.Acceleration ; %d\n", PVDFormat.Acceleration);
  	printf("PVDFormat.DTC ; %u\n", PVDFormat.DTC);
  	printf("PVDFormat.EngineTemp ; %u\n", PVDFormat.EngineTemp);
  	printf("PVDFormat.CoolantTemp ; %u\n", PVDFormat.CoolantTemp);
  	printf("PVDFormat.BatteryTemp ; %u\n", PVDFormat.BatteryTemp);
  	printf("PVDFormat.BatteryWarning ; %u\n", PVDFormat.BatteryWarning);
  	printf("PVDFormat.TPMSWarning ; %u\n", PVDFormat.TPMSWarning);
  	printf("PVDFormat.ABSWarning ; %u\n", PVDFormat.ABSWarning);
  	printf("PVDFormat.BrakeWarning ; %u\n", PVDFormat.BrakeWarning);
*/

  //printf(" test : 0x%x , test2 : 0x%x, test3 : 0x%x, test4 : 0x%x \n", test.test1, test.test2, test.test3, test.test4);
  	close(sockfd_AICM);

  return 0;
}

/*
int LinuxEther::UDPTxInit_CANOE(void)
{
	
	// open UDP_tx	
	if((this->sockfd_CANOE = socket(AF_INET, SOCK_DGRAM, 0)) < 0){
        perror("socket ");
        return 1;
    }	
	else
		printf("socket sucessful\n");

  	setsockopt(this->sockfd_CANOE, SOL_SOCKET, SO_BROADCAST, &enabled, sizeof(enabled));
	printf("1setsockopt sucessful\n");
 	CANOE_addr.sin_family = AF_INET;
	CANOE_addr.sin_addr.s_addr = inet_addr("192.168.100.40" ); //AICM 占쌍쇽옙
//	AICM_addr.sin_addr.s_addr = inet_addr("255.255.255.255" ); //AICM 占쌍쇽옙
	CANOE_addr.sin_port = htons(2002);//2001
	printf("2setsockopt sucessful\n");

	bind(this->sockfd_CANOE, (struct sockaddr *)&this->CANOE_addr, sizeof(this->CANOE_addr));
	printf("bind sucessful\n");
}

int LinuxEther::UDPTxFunc_CANOE(void)
{
	LinuxEther *t_tx;
//    t_rx = (LinuxEther *)temp;

//  	PVDFormatGeneration();
	memset(&t_tx->UDPTransBuffer, 0xff, sizeof(char)*1024);
	printf("Buffer ready to transmit \n");
  	if(sendto(t_tx->sockfd_CANOE, UDPTxBuffer, sizeof(UDPTxBuffer), 0,(struct sockaddr *)&t_tx->CANOE_addr, sizeof(t_tx->CANOE_addr)) <0)
  	{
  		printf("send fail %d\n");
  		exit (1);
  	}
  	else printf("send result: %d\n", send);

  	CheckCnt++;
  	printf("CANOE test check ; %d\n", CheckCnt);

  	if ( CheckCnt > 100)
  		CheckCnt = 0;
	close(sockfd_CANOE);

  return 0;
}

*/
int LinuxEther::AVTPTxInit(void)
{
	if ((AVTPTransmit = socket(PF_PACKET, SOCK_RAW, htons(0x22f0))) < 0) {
		printf("Error: could not open socket\n");
		//return -1;
	}

	struct ifreq buffer;
	int ifindex;
	memset(&buffer, 0x00, sizeof(buffer));
	strncpy(buffer.ifr_name, iface, IFNAMSIZ);
	if (ioctl(AVTPTransmit, SIOCGIFINDEX, &buffer) < 0) {
		printf("Error: could not get interface index\n");
		close(AVTPTransmit);
		//return -1;
	}
	ifindex = buffer.ifr_ifindex;

	if (ioctl(AVTPTransmit, SIOCGIFHWADDR, &buffer) < 0) {
		printf("Error: could not get interface address\n");
		close(AVTPTransmit);
		//return -1;
	}
	memcpy((void*)source, (void*)(buffer.ifr_hwaddr.sa_data), ETH_ALEN);

	memset((void*)&saddrll, 0, sizeof(saddrll));
	saddrll.sll_family = PF_PACKET;
	saddrll.sll_ifindex = ifindex;
	saddrll.sll_halen = ETH_ALEN;
	memcpy((void*)(saddrll.sll_addr), (void*)dest, ETH_ALEN);

	AVTPStreamHeader[4] = buffer.ifr_hwaddr.sa_data[0];
	AVTPStreamHeader[5] = buffer.ifr_hwaddr.sa_data[1];
	AVTPStreamHeader[6] = buffer.ifr_hwaddr.sa_data[2];
	AVTPStreamHeader[7] = buffer.ifr_hwaddr.sa_data[3];
	AVTPStreamHeader[8] = buffer.ifr_hwaddr.sa_data[4];
	AVTPStreamHeader[9] = buffer.ifr_hwaddr.sa_data[5];

	AVTPControlHeader[4] = buffer.ifr_hwaddr.sa_data[0];
	AVTPControlHeader[5] = buffer.ifr_hwaddr.sa_data[1];
	AVTPControlHeader[6] = buffer.ifr_hwaddr.sa_data[2];
	AVTPControlHeader[7] = buffer.ifr_hwaddr.sa_data[3];
	AVTPControlHeader[8] = buffer.ifr_hwaddr.sa_data[4];
	AVTPControlHeader[9] = buffer.ifr_hwaddr.sa_data[5];
}


int LinuxEther::AVTPTxFunc(void)
{
	for (i=0; i<100; i++)
		DataPayload[i] = UDPRxBuffer[i];

	AVTP_header_len = ETH_AVTP_CONTROL_LEN;
	DataPayloadLength = sizeof(DataPayload);

//	memcpy(ControlFrame.Field.Header.HeaderDest, dest, ETH_ALEN);
//	memcpy(ControlFrame.Field.Header.HeaderSource, source, ETH_ALEN);
//	ControlFrame.Field.Header.HeaderProto = htons(0x22f0);
	memcpy(ControlFrame.Field.AVTPHeader, AVTPControlHeader, AVTP_header_len);
	memcpy(ControlFrame.Field.Payload, DataPayload, DataPayloadLength);

	FrameLength = DataPayloadLength + AVTP_header_len + ETH_HLEN;
	sendto(AVTPTransmit, ControlFrame.Buffer, FrameLength, 0, (struct sockaddr*)&saddrll, sizeof(saddrll));
}


/*
void *UDP::udp_tx_func(void)
//void *UDP::udp_tx_func(void *temp)
{

//	int send_AICM;
	UDP *t_tx;
 	memset(&t_tx, 0x00, sizeof(t_tx));
//    t_tx = (UDP *)temp;
		int send_AICM = 0;

//  socklen_t tx_addr_len;    	

	printf("send messages\n");

  		test.test1 = 0x1111;
	  	test.test2 = 0x2222;
	  	test.test3 = 0x33;
//	  	test.test4 = rand()%16;
	  	test.test4 = ;

//	for (;;)
//	{	

//  		send_AICM = sendto(t_tx->sockfd_AICM, &test, sizeof(test), 0,(struct sockaddr *)&t_tx->tx_addr, sizeof(t_tx->tx_addr));
  		sendto(t_tx->sockfd_AICM, &test, sizeof(test), 0,(struct sockaddr *)&t_tx->tx_addr, sizeof(t_tx->tx_addr));

      printf(" test1 : 0x%x , test2 : 0x%x, test3 : 0x%x, test4 : 0x%x \n", test.test1, test.test2, test.test3, test.test4); 
//		send_AICM = sendto(sockfd_AICM, &'\0', sizeof((char)*100), 0,(struct sockaddr *)&tx_addr, sizeof(tx_addr));
//      test.test4 ++;
//		usleep(1000000);
//	}

//     if(bind(this->rx, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
//		memset(&tthis->recv_buffer, 0x00, sizeof(char)*1024);
//		rx_addr_len = sizeof(tthis->client_addr);
//		tthis->urx = recvfrom(tthis->rx, tthis->recv_buffer, 1024, 0, (struct sockaddr *)&tthis->client_addr, &rx_addr_len);

  return 0;
}
*/ 


int LinuxEther::RxInit()
{
	// Open UDP Port for V2XTIM
	if((this->UDPRxV2XTIM = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
        perror("socket ");
        return -1;
    }

	memset(&rx_addr, 0x00, sizeof(rx_addr));
    rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
    rx_addr.sin_port = htons(V2XTIMRXPORT);
    if(bind(this->UDPRxV2XTIM, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
        perror("bind ");
        return 1;
    }
    printf("UDP V2XTIM Socket has been created.\n");

	// Open UDP Port for V2XSPaT
	if((this->UDPRxV2XSPaT = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
        perror("socket ");
        return -1;
    }

	memset(&rx_addr, 0x00, sizeof(rx_addr));
    rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
    rx_addr.sin_port = htons(V2XSPaTRXPORT);
    if(bind(this->UDPRxV2XSPaT, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
        perror("bind ");
        return 1;
    }
    printf("UDP V2XSPaT Socket has been created.\n");

    // Open UDP Port for MAP
	if((this->UDPRxMAP = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(MAPRXPORT);
	if(bind(this->UDPRxMAP, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP MAP Socket has been created.\n");

    // Open UDP Port for GPS
	if((this->UDPRxGPS = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(GPSRXPORT);
	if(bind(this->UDPRxGPS, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP GPS Socket has been created.\n");

	// Open UDP Port for GPSTime
	if((this->UDPRxGPSTime = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(GPSTimeRXPORT);
	if(bind(this->UDPRxGPSTime, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP GPSTime Socket has been created.\n");

    // Open UDP Port for LPM
	if((this->UDPRxLPM = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(LPMRXPORT);
	if(bind(this->UDPRxLPM, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP LPM Socket has been created.\n");

	// Open UDP Port between Canoe and target board
	if((this->UDPRxCANOE = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.99" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(CANOERXPORT);
	if(bind(this->UDPRxCANOE, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP CANOE Socket has been created.\n");
/*
	// Open UDP Port for target board and PC 
	if((this->UDPRxCANOE1 = socket(AF_INET, SOCK_DGRAM, 0)) <0)
	{
		perror("socket ");
		return -1;
	}

	memset(&rx_addr, 0x00, sizeof(rx_addr));
	rx_addr.sin_family = AF_INET;
	rx_addr.sin_addr.s_addr = inet_addr("192.168.100.40" );  //htonl(INADDR_ANY);
//	rx_addr.sin_addr.s_addr = inet_addr("255.255.255.255" );  //htonl(INADDR_ANY);
	rx_addr.sin_port = htons(CANOERXPORT1);
	if(bind(this->UDPRxCANOE1, (struct sockaddr *)&rx_addr, sizeof(rx_addr)) < 0)
	{
		perror("bind ");
		return 1;
	}
	printf("UDP PC Socket has been created.\n");
*/


    // open AVTP_rx
	if ((this->AVTPRx=socket(PF_PACKET, SOCK_RAW, htons(0x22F0))) == -1) {
		perror("socket");
		return 0;
	}
	printf("AVTP Socket has been created.\n");

	memset(&ifr, 0, sizeof(ifr));
	strcpy(ifr.ifr_name, iface);

	/*
		if ((ioctl(sfd, SIOCGIFINDEX, &ifr)) == -1) {
			strcpy(ifr.ifr_name, IFRNAME1);
			if ((ioctl(sfd, SIOCGIFINDEX, &ifr)) == -1) {
				perror("ioctl 1");
				close(sfd);
				return 0;
			}
		}
	*/

	memset(&sll, 0, sizeof(sll));
	sll.sll_family = PF_PACKET;
	sll.sll_ifindex =  ifr.ifr_ifindex;
	//sll.sll_ifindex =  2;
	sll.sll_protocol = htons(0x22F0);
	AVTPlength = sizeof(sll);

	if ((bind(this->AVTPRx, (struct sockaddr *)&sll, sizeof(sll))) == -1) {
		perror("bind");
		close(this->AVTPRx);
		return 0;
    }


	return 0;
}

void LinuxEther::ReceiveThread(void)
{
	this->ThrID_AVTP_Rx = pthread_create(&this->AVTPRecvThread, NULL, this->AVTPRxFunc, (void *) this);
	this->ThrID_UDPV2XTIM_Rx = pthread_create(&this->UDPV2XTIMRecvThread, NULL, this->UDPV2XTIMRxFunc, (void *) this);
	this->ThrID_UDPV2XSPaT_Rx = pthread_create(&this->UDPV2XSPaTRecvThread, NULL, this->UDPV2XSPaTRxFunc, (void *) this);
	this->ThrID_UDPMAP_Rx = pthread_create(&this->UDPMAPRecvThread, NULL, this->UDPMAPRxFunc, (void *) this);
	this->ThrID_UDPGPS_Rx = pthread_create(&this->UDPGPSRecvThread, NULL, this->UDPGPSRxFunc, (void *) this);
	this->ThrID_UDPGPSTime_Rx = pthread_create(&this->UDPGPSTimeRecvThread, NULL, this->UDPGPSTimeRxFunc, (void *) this);
	this->ThrID_UDPLPM_Rx = pthread_create(&this->UDPLPMRecvThread, NULL, this->UDPLPMRxFunc, (void *) this);
	this->ThrID_UDPCANOE_Rx = pthread_create(&this->UDPCANOERecvThread, NULL, this->UDPCANOERxFunc, (void *) this);	
}


void *LinuxEther::UDPV2XTIMRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP V2X Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxV2XTIM = recvfrom(t_rx->UDPRxV2XTIM, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);

		TIMFormat.msgCnt = t_rx->UDPRecvBuffer[0];
		TIMFormat.frameType = t_rx->UDPRecvBuffer[1];
		TIMFormat.furtherInfoID = t_rx->UDPRecvBuffer[2] + (t_rx->UDPRecvBuffer[3]<<8);
		TIMFormat.roadSignLatitude = t_rx->UDPRecvBuffer[4] + (t_rx->UDPRecvBuffer[5]<<8) + (t_rx->UDPRecvBuffer[6]<<16) + (t_rx->UDPRecvBuffer[7]<<24);
		TIMFormat.roadSignLongitude = t_rx->UDPRecvBuffer[8] + (t_rx->UDPRecvBuffer[9]<<8) + (t_rx->UDPRecvBuffer[10]<<16) + (t_rx->UDPRecvBuffer[11]<<24);
		TIMFormat.headingSlice = t_rx->UDPRecvBuffer[12] + (t_rx->UDPRecvBuffer[13]<<8);

		TIMFormat.startYear = t_rx->UDPRecvBuffer[14] + (t_rx->UDPRecvBuffer[15]<<8);
		TIMFormat.startTime = t_rx->UDPRecvBuffer[16] + (t_rx->UDPRecvBuffer[17]<<8) + (t_rx->UDPRecvBuffer[18]<<16) + (t_rx->UDPRecvBuffer[19]<<24);
		TIMFormat.durationTime = t_rx->UDPRecvBuffer[20] + (t_rx->UDPRecvBuffer[21]<<8);
		TIMFormat.priority = t_rx->UDPRecvBuffer[22];
		TIMFormat.anchorLatitude = t_rx->UDPRecvBuffer[23] + (t_rx->UDPRecvBuffer[24]<<8) + (t_rx->UDPRecvBuffer[25]<<16) + (t_rx->UDPRecvBuffer[26]<<24);
		TIMFormat.anchorLongitude = t_rx->UDPRecvBuffer[27] + (t_rx->UDPRecvBuffer[28]<<8) + (t_rx->UDPRecvBuffer[29]<<16) + (t_rx->UDPRecvBuffer[30]<<24);
		TIMFormat.anchorElevation = t_rx->UDPRecvBuffer[31] + (t_rx->UDPRecvBuffer[32]<<8);
		TIMFormat.LaneWidth = t_rx->UDPRecvBuffer[33] + (t_rx->UDPRecvBuffer[34]<<8);
		TIMFormat.directionality = t_rx->UDPRecvBuffer[35];
		TIMFormat.pathLatitude = t_rx->UDPRecvBuffer[36] + (t_rx->UDPRecvBuffer[37]<<8) + (t_rx->UDPRecvBuffer[38]<<16) + (t_rx->UDPRecvBuffer[39]<<24);
		TIMFormat.pathLongitude = t_rx->UDPRecvBuffer[40] + (t_rx->UDPRecvBuffer[41]<<8) + (t_rx->UDPRecvBuffer[42]<<16) + (t_rx->UDPRecvBuffer[43]<<24);
		TIMFormat.RegionalcontentType = t_rx->UDPRecvBuffer[44];
		TIMFormat.Regionaltype = t_rx->UDPRecvBuffer[45];
		for (i=0; i<512; i++)
		{
			TIMFormat.RegionalValue[i] = t_rx->UDPRecvBuffer[46+i];
		}
		AliveV2XTIM++;
		if (AliveV2XTIM == 16)
			AliveV2XTIM = 0;

		AliveESU1++;
		if (AliveESU1 == 16)
			AliveESU1 = 0;

		AliveESU2++;
		if (AliveESU2 == 16)
			AliveESU2 = 0;

		printf("TIMFormat.LaneWidth ; %u\n", TIMFormat.LaneWidth);
	  	/*
	  	printf("TIMFormat.frameType ; %u\n", TIMFormat.frameType);
	  	printf("TIMFormat.furtherInfoID ; %u\n", TIMFormat.furtherInfoID);
	  	printf("TIMFormat.roadSignLatitude ; %u\n", TIMFormat.roadSignLatitude);
	  	printf("TIMFormat.roadSignLongitude ; %u\n", TIMFormat.roadSignLongitude);
	  	printf("TIMFormat.headingSlice ; %u\n", TIMFormat.headingSlice);
	  	printf("TIMFormat.startYear ; %u\n", TIMFormat.startYear);
	  	printf("TIMFormat.startTime ; %u\n", TIMFormat.startTime);
	  	printf("TIMFormat.durationTime ; %u\n", TIMFormat.durationTime);
	  	printf("TIMFormat.priority ; %u\n", TIMFormat.priority);
	  	printf("TIMFormat.anchorLatitude ; %u\n", TIMFormat.anchorLatitude);
	  	printf("TIMFormat.anchorLongitude ; %u\n", TIMFormat.anchorLongitude);
	  	printf("TIMFormat.anchorElevation ; %u\n", TIMFormat.anchorElevation);
	  	printf("TIMFormat.LaneWidth ; %u\n", TIMFormat.LaneWidth);
	  	printf("TIMFormat.directionality ; %u\n", TIMFormat.directionality);
	  	printf("TIMFormat.pathLatitude ; %u\n", TIMFormat.pathLatitude);
	  	printf("TIMFormat.pathLongitude ; %u\n", TIMFormat.pathLongitude);
	  	printf("TIMFormat.RegionalcontentType ; %u\n", TIMFormat.RegionalcontentType);
	  	printf("TIMFormat.Regionaltype ; %u\n", TIMFormat.Regionaltype);
	  	for (i=0; i<512; i++)
	  	{
	  		printf("TIMFormat.RegionalValue[%d] ; %u\n", i, TIMFormat.RegionalValue[i]);
	  	}
*/

	}
}

void *LinuxEther::UDPV2XSPaTRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP V2X Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxV2XSPaT = recvfrom(t_rx->UDPRxV2XSPaT, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);

	  	for (i=0; i<63; i++)
	  	{
	  		SPaTFormat.name[i] = t_rx->UDPRecvBuffer[i];
	  	}
	  	SPaTFormat.regionID = t_rx->UDPRecvBuffer[64] + (t_rx->UDPRecvBuffer[65]<<8);
	  	SPaTFormat.IntersectionID = t_rx->UDPRecvBuffer[66] + (t_rx->UDPRecvBuffer[67]<<8);
	  	SPaTFormat.revision = t_rx->UDPRecvBuffer[68];
	  	SPaTFormat.status = t_rx->UDPRecvBuffer[69] + (t_rx->UDPRecvBuffer[70]<<8);
	  	SPaTFormat.MovementStateCnt = t_rx->UDPRecvBuffer[71];

	  	if (SPaTFormat.MovementStateCnt == 0)
	  	{
	  		for (i=0; i<63; i++)
			{
				SPaTFormat.movementName[0][i] = 0;
			}
	  		SPaTFormat.signalGroupID[0] = 0;
			SPaTFormat.MovementEventCnt[0] = 0;
			SPaTFormat.eventState[0] = 0;
			SPaTFormat.minEndTime[0] = 0;
			SPaTFormat.ManeuverAssistCnt[0] = 0;
			SPaTFormat.pedbicycleDetect[0] = 0;
	  	}
	  	else
	  	{
	  		for (k=0;k<SPaTFormat.MovementStateCnt;k++)
			{
				for (i=0; i<63; i++)
				{
					SPaTFormat.movementName[k][i] = t_rx->UDPRecvBuffer[i+(72+(64*k))];
				}
				SPaTFormat.signalGroupID[k] = t_rx->UDPRecvBuffer[136+(64*k)];
				SPaTFormat.MovementEventCnt[k] = t_rx->UDPRecvBuffer[137+(64*k)];
				SPaTFormat.eventState[k] = t_rx->UDPRecvBuffer[138+(64*k)];
				SPaTFormat.minEndTime[k] = t_rx->UDPRecvBuffer[139+(64*k)] + (t_rx->UDPRecvBuffer[140+(64*k)]<<8);
				SPaTFormat.ManeuverAssistCnt[k] = t_rx->UDPRecvBuffer[141+(64*k)];
				SPaTFormat.pedbicycleDetect[k] = t_rx->UDPRecvBuffer[142+(64*k)];
			}
	  	}

	  	printf("SPaTFormat.eventState ; %u\n", SPaTFormat.eventState);

		AliveV2XSPaT++;
		if (AliveV2XSPaT == 16)
			AliveV2XSPaT = 0;

		AliveESU1++;
		if (AliveESU1 == 16)
			AliveESU1 = 0;

		AliveESU2++;
		if (AliveESU2 == 16)
			AliveESU2 = 0;
/*
	  	for (i=0; i<63; i++)
	  	{
	  		printf("SPaTFormat.name[%d] ; %u\n", i, SPaTFormat.name[i]);
	  	}

	  	printf("SPaTFormat.regionID ; %u\n", SPaTFormat.regionID);
	  	printf("SPaTFormat.IntersectionID ; %u\n", SPaTFormat.IntersectionID);
	  	printf("SPaTFormat.revision ; %u\n", SPaTFormat.revision);
	  	printf("SPaTFormat.status ; %u\n", SPaTFormat.status);
	  	printf("SPaTFormat.MovementStateCnt ; %u\n", SPaTFormat.MovementStateCnt);
	  	for (i=0; i<63; i++)
	  	{
	  		printf("SPaTFormat.movementName[%d] ; %u\n", i, SPaTFormat.movementName[i]);
	  	}
	  	printf("SPaTFormat.signalGroupID ; %u\n", SPaTFormat.signalGroupID);
	  	printf("SPaTFormat.MovementEventCnt ; %u\n", SPaTFormat.MovementEventCnt);
	  	printf("SPaTFormat.eventState ; %u\n", SPaTFormat.eventState);
	  	printf("SPaTFormat.minEndTime ; %u\n", SPaTFormat.minEndTime);
	  	printf("SPaTFormat.ManeuverAssistCnt ; %u\n", SPaTFormat.ManeuverAssistCnt);
	  	printf("SPaTFormat.pedbicycleDetect ; %u\n", SPaTFormat.pedbicycleDetect);
*/
	}
}

void *LinuxEther::UDPMAPRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for MAP Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP MAP Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxMAP = recvfrom(t_rx->UDPRxMAP, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);
		printf("Received data_MAP\n");

		AliveV2XMAP++;
		if (AliveV2XMAP == 16)
			AliveV2XMAP = 0;

		
	}
}

void *LinuxEther::UDPCANOERxFunc(void *temp)
{
	LinuxEther *t_rx;
	LinuxEther *t_tx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for CANOE Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP CANOE Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxCANOE = recvfrom(t_rx->UDPRxCANOE, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);
		printf("Received data_CANOE of size %d\n", t_rx->uUDPRxCANOE);

		if (t_rx->uUDPRxCANOE == 100){

			printf("Ethernet connection NOK - UDP message of payload size 500 bytes will be sent\n");
//			this->ThrID_UDPCANOE1_Rx = pthread_create(&this->UDPCANOE1RecvThread, NULL, this->UDPCANOE1RxFunc, (void *) this);
//			printf("Thread calling to send packet to PC is done");

			memset(&t_tx->UDPTransBuffer, 0x55, sizeof(char)*1024);
			printf("Buffer ready to transmit \n");
//			t_tx->UDPTxFunc_CANOE();
			printf("usleep added to adjust the delay\n");
			usleep(47000); //add 47000 microsecond/47 milisecond of delay 

			if (sendto(t_rx->UDPRxCANOE, t_tx->UDPTransBuffer, sizeof(t_tx->UDPTransBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, rx_addr_len) == -1)
			{
				printf("Send fail\n");
				exit(1);
			}
			else
				printf("Send successful\n");

		}
		else
			printf("Ethernet connection OK - UDP message will be bypassed\n");

//		AliveV2XMAP++;
//		if (AliveV2XMAP == 16)
//			AliveV2XMAP = 0;

		
	}
}
/*
void *LinuxEther::UDPCANOE1RxFunc(void *temp)
{

	LinuxEther *t_tx;
    t_tx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for CANOE Ethernet(UDP) messages\n");

	for (;;)
	{
		memset(&t_tx->UDPTransBuffer, 0x00, sizeof(char)*1024);

		printf("Buffer ready to transmit \n");

		if (sendto(t_tx->UDPRxCANOE1, t_tx->UDPTransBuffer, sizeof(t_tx->UDPTransBuffer), 0, (struct sockaddr *)&t_tx->rx_addr, tx_addr_len) == -1)
		{
			printf("Send fail\n");
			exit(1);
		}
		else
			printf("Send successful\n");
	}


//		AliveV2XMAP++;
//		if (AliveV2XMAP == 16)
//			AliveV2XMAP = 0;

		

}
*/

void *LinuxEther::UDPGPSRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for GPS Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP MAP Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxGPS = recvfrom(t_rx->UDPRxGPS, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);
		printf("Received data_GPS\n");

		RawVehicleLatitude = t_rx->UDPRecvBuffer[0] + (t_rx->UDPRecvBuffer[1]<<8) + (t_rx->UDPRecvBuffer[2]<<16) + (t_rx->UDPRecvBuffer[3]<<24);
		VehicleLatitude = RawVehicleLatitude * 0.0000001; //Deg
		RawVehicleLongitude = t_rx->UDPRecvBuffer[4] + (t_rx->UDPRecvBuffer[5]<<8) + (t_rx->UDPRecvBuffer[6]<<16) + (t_rx->UDPRecvBuffer[7]<<24);
		VehicleLongitude = RawVehicleLongitude * 0.0000001; //Deg
		RawVehicleAltidude = t_rx->UDPRecvBuffer[8] + (t_rx->UDPRecvBuffer[9]<<8) + (t_rx->UDPRecvBuffer[10]<<16) + (t_rx->UDPRecvBuffer[11]<<24);
		VehicleAltidude = RawVehicleAltidude * 0.0000001; //Deg

		RawVehicleNorthVelocity = t_rx->UDPRecvBuffer[12] + (t_rx->UDPRecvBuffer[13]<<8) + (t_rx->UDPRecvBuffer[14]<<16) + (t_rx->UDPRecvBuffer[15]<<24);
		VehicleNorthVelocity = RawVehicleNorthVelocity * 0.000001; //KPH
		RawVehicleEastVelocity = t_rx->UDPRecvBuffer[16] + (t_rx->UDPRecvBuffer[17]<<8) + (t_rx->UDPRecvBuffer[18]<<16) + (t_rx->UDPRecvBuffer[19]<<24);
		VehicleEastVelocity = RawVehicleEastVelocity * 0.000001; //KPH
		RawVehicleDownVelocity = t_rx->UDPRecvBuffer[20] + (t_rx->UDPRecvBuffer[21]<<8) + (t_rx->UDPRecvBuffer[22]<<16) + (t_rx->UDPRecvBuffer[23]<<24);
		VehicleDownVelocity = RawVehicleDownVelocity * 0.000001; //KPH

		RawVehicleRoll = t_rx->UDPRecvBuffer[24] + (t_rx->UDPRecvBuffer[25]<<8) + (t_rx->UDPRecvBuffer[26]<<16) + (t_rx->UDPRecvBuffer[27]<<24);
		VehicleRoll = RawVehicleRoll * 0.000001; //Deg
		RawVehiclePitch = t_rx->UDPRecvBuffer[28] + (t_rx->UDPRecvBuffer[29]<<8) + (t_rx->UDPRecvBuffer[30]<<16) + (t_rx->UDPRecvBuffer[31]<<24);
		VehiclePitch = RawVehiclePitch * 0.000001; //Deg
		RawVehicleHeading = t_rx->UDPRecvBuffer[32] + (t_rx->UDPRecvBuffer[33]<<8) + (t_rx->UDPRecvBuffer[34]<<16) + (t_rx->UDPRecvBuffer[35]<<24);
		VehicleHeading = RawVehicleHeading * 0.000001; //Deg

		RawVehicleHDLReadType = t_rx->UDPRecvBuffer[36];
		RawVehicleLongitudinalHDL = t_rx->UDPRecvBuffer[37] + (t_rx->UDPRecvBuffer[38]<<8);
		VehicleLongitudinalHDL = RawVehicleLongitudinalHDL * 0.01; //m
		RawVehicleLateralHDL = t_rx->UDPRecvBuffer[39] + (t_rx->UDPRecvBuffer[40]<<8);
		VehicleLateralHDL = RawVehicleLateralHDL * 0.01; //m
		RawVehicleHDLWarning = t_rx->UDPRecvBuffer[41];

		AliveGPS++;
		if (AliveGPS == 16)
			AliveGPS = 0;

//		printf("received data_GPS ; %s\n", t_rx->UDPRecvBuffer);
	}
}

void *LinuxEther::UDPGPSTimeRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP MAP Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxGPSTime = recvfrom(t_rx->UDPRxGPSTime, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);

		AliveGPSTime++;
		if (AliveGPSTime == 16)
			AliveGPSTime = 0;
		/*
		RawGPSTimeStamp = t_rx->UDPRecvBuffer[0] + (t_rx->UDPRecvBuffer[1]<<8) + (t_rx->UDPRecvBuffer[2]<<16) + (t_rx->UDPRecvBuffer[3]<<24) + (t_rx->UDPRecvBuffer[4]<<32)
						+ (t_rx->UDPRecvBuffer[5]<<40) + (t_rx->UDPRecvBuffer[6]<<48) + (t_rx->UDPRecvBuffer[7]<<56) + (t_rx->UDPRecvBuffer[8]<<64)+ (t_rx->UDPRecvBuffer[9]<<72)
						+ (t_rx->UDPRecvBuffer[10]<<80) + (t_rx->UDPRecvBuffer[11]<<88)+ (t_rx->UDPRecvBuffer[12]<<96);
*/
	}
}

void *LinuxEther::UDPLPMRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

	socklen_t rx_addr_len;
	printf("Waiting for Ethernet(UDP) messages\n");

	for (;;)
	{
		//UDP MAP Frame Recv
		memset(&t_rx->UDPRecvBuffer, 0x00, sizeof(char)*1024);
		rx_addr_len = sizeof(t_rx->rx_addr);
		t_rx->uUDPRxLPM = recvfrom(t_rx->UDPRxLPM, t_rx->UDPRecvBuffer, sizeof(t_rx->UDPRecvBuffer), 0, (struct sockaddr *)&t_rx->rx_addr, &rx_addr_len);

		AliveLiDAR_FL++;
		if (AliveLiDAR_FL == 16)
			AliveLiDAR_FL = 0;

		AliveLiDAR_FR++;
		if (AliveLiDAR_FR == 16)
			AliveLiDAR_FR = 0;

		AliveLiDAR_VFOV_F++;
		if (AliveLiDAR_VFOV_F == 16)
			AliveLiDAR_VFOV_F = 0;

		AliveLiDAR_VFOV_L++;
		if (AliveLiDAR_VFOV_L == 16)
			AliveLiDAR_VFOV_L = 0;

		AliveLiDAR_VFOV_R++;
		if (AliveLiDAR_VFOV_R == 16)
			AliveLiDAR_VFOV_R = 0;

		AliveLiDAR_RL++;
		if (AliveLiDAR_RL == 16)
			AliveLiDAR_RL = 0;

		AliveLiDAR_RR++;
		if (AliveLiDAR_RR == 16)
			AliveLiDAR_RR = 0;

		AliveLPM++;
		if (AliveLPM == 16)
			AliveLPM = 0;
	}
}

void *LinuxEther::AVTPRxFunc(void *temp)
{
	LinuxEther *t_rx;
    t_rx = (LinuxEther *)temp;

//	socklen_t rx_addr_len;
	printf("Waiting for Ethernet(AVTP) messages\n");


	for (;;)
	{
		//AVTP Frame Recv
		t_rx->uAVTPRx = recvfrom(t_rx->AVTPRx, t_rx->AVTPRecvBuffer, sizeof(t_rx->AVTPRecvBuffer), 0, (struct sockaddr *)&sll, &AVTPlength);
		if ((sizeof(t_rx->AVTPRecvBuffer)) > 10)
		{
			for(i=0; i<150; i++)
				AVTPRxBuffer[i] = t_rx->AVTPRecvBuffer[i];
			//printf("received data ; %d", AVTPRxBuffer[26]);
			for(i=0; i<100; i++)
					printf("-%d", AVTPRxBuffer[27+i]);
					printf("\n");
		}
		t_rx->AVTPRecvBuffer[t_rx->AVTPRx] = '\0';
	}

}

void PVDFormatGeneration(void)
{
	PVDFormat.GPS_UTC_Time=20190827;
	PVDFormat.Longitude=CurrentVehicleLongitude;
	PVDFormat.Latitude=CurrentVehicleLatitude;
	PVDFormat.Elevation=2325;
	PVDFormat.Heading=28800;

	if (GearPosition == 0)
		PVDFormat.Transmission = 1;
	else if (GearPosition == 5)
		PVDFormat.Transmission = 2;
	else if (GearPosition == 6)
		PVDFormat.Transmission = 0;
	else if (GearPosition == 7)
		PVDFormat.Transmission = 3;
	else
		PVDFormat.Transmission = 7;

	PVDFormat.Speed=(VS*14);
	PVDFormat.PosAccuracy=0;

	PVDFormat.Event=0; //자율주행 Event

	if (HeadLightStatus == 1)
		PVDFormat.ExteriorLight = 1;
	else if (RightTurnStatus == 1)
		PVDFormat.ExteriorLight = 4;
	else if (LeftTurnStatus == 1)
		PVDFormat.ExteriorLight = 3;
	else
		PVDFormat.ExteriorLight=0;

	PVDFormat.LightBar=0;
	PVDFormat.BrakeStatus=Fb_Brake_Press1;
	PVDFormat.SteeringAngle=CurrentSteeringAngle/1.5;
	PVDFormat.YawRate=YawRate*100; //0.01deg/s
	PVDFormat.ThrottlePosition=AccelPedPos*2; //쓰로틀 위치 unit 0.5%
	PVDFormat.GNSSStatus=1; //GNSS 상태

	PVDFormat.DrivingMode = ACCAct;
	PVDFormat.Horn = 0;
	PVDFormat.Fallback = 0;
	PVDFormat.ValidationEventId = 0;
	PVDFormat.ValidationEventValue = 0;
	PVDFormat.ValidationEventDetail = 0;
	PVDFormat.SOC = 0;
	PVDFormat.H2 = 0;
	PVDFormat.Odometer = 12315;
	PVDFormat.Tripmeter = 2315;
	PVDFormat.distanceToEmpty = 482;

	if (RightTurnStatus == 1)
		PVDFormat.TurnSignal = 2;
	else if (LeftTurnStatus == 1)
		PVDFormat.TurnSignal = 1;
	else
		PVDFormat.TurnSignal = 0;

	PVDFormat.PowerGauge = -20;
	PVDFormat.CurLane = 2;
	PVDFormat.Torque = StrTorque*10;
	PVDFormat.Acceleration = LongAccel*9.81*100; //0.01m/s2
	PVDFormat.DTC = 0;
	PVDFormat.EngineTemp = 34;
	PVDFormat.CoolantTemp = 24;
	PVDFormat.BatteryTemp = 36;
	PVDFormat.BatteryWarning = 0;
	PVDFormat.TPMSWarning = 0;
	PVDFormat.ABSWarning = 0;
	PVDFormat.BrakeWarning = 0;

	UDPTxBuffer[0] = PVDFormat.GPS_UTC_Time>>24;
	UDPTxBuffer[1] = PVDFormat.GPS_UTC_Time>>16;
	UDPTxBuffer[2] = PVDFormat.GPS_UTC_Time>>8;
	UDPTxBuffer[3] = PVDFormat.GPS_UTC_Time;

	UDPTxBuffer[4] = PVDFormat.Longitude>>24;
	UDPTxBuffer[5] = PVDFormat.Longitude>>16;
	UDPTxBuffer[6] = PVDFormat.Longitude>>8;
	UDPTxBuffer[7] = PVDFormat.Longitude;

	UDPTxBuffer[8] = PVDFormat.Latitude>>24;
	UDPTxBuffer[9] = PVDFormat.Latitude>>16;
	UDPTxBuffer[10] = PVDFormat.Latitude>>8;
	UDPTxBuffer[11] = PVDFormat.Latitude;

	UDPTxBuffer[12] = PVDFormat.Elevation>>24;
	UDPTxBuffer[13] = PVDFormat.Elevation>>16;
	UDPTxBuffer[14] = PVDFormat.Elevation>>8;
	UDPTxBuffer[15] = PVDFormat.Elevation;

	UDPTxBuffer[16] = PVDFormat.Heading>>24;
	UDPTxBuffer[17] = PVDFormat.Heading>>16;
	UDPTxBuffer[18] = PVDFormat.Heading>>8;
	UDPTxBuffer[19] = PVDFormat.Heading;

	UDPTxBuffer[20] = PVDFormat.Transmission>>8;
	UDPTxBuffer[21] = PVDFormat.Transmission;
	UDPTxBuffer[22] = PVDFormat.Speed>>8;
	UDPTxBuffer[23] = PVDFormat.Speed;

	UDPTxBuffer[24] = PVDFormat.PosAccuracy>>24;
	UDPTxBuffer[25] = PVDFormat.PosAccuracy>>16;
	UDPTxBuffer[26] = PVDFormat.PosAccuracy>>8;
	UDPTxBuffer[27] = PVDFormat.PosAccuracy;

	UDPTxBuffer[28] = PVDFormat.Event>>24;
	UDPTxBuffer[29] = PVDFormat.Event>>16;
	UDPTxBuffer[30] = PVDFormat.Event>>8;
	UDPTxBuffer[31] = PVDFormat.Event;

	UDPTxBuffer[32] = PVDFormat.ExteriorLight>>24;
	UDPTxBuffer[33] = PVDFormat.ExteriorLight>>16;
	UDPTxBuffer[34] = PVDFormat.ExteriorLight>>8;
	UDPTxBuffer[35] = PVDFormat.ExteriorLight;

	UDPTxBuffer[36] = PVDFormat.LightBar>>24;
	UDPTxBuffer[37] = PVDFormat.LightBar>>16;
	UDPTxBuffer[38] = PVDFormat.LightBar>>8;
	UDPTxBuffer[39] = PVDFormat.LightBar;

	UDPTxBuffer[40] = PVDFormat.BrakeStatus>>24;
	UDPTxBuffer[41] = PVDFormat.BrakeStatus>>16;
	UDPTxBuffer[42] = PVDFormat.BrakeStatus>>8;
	UDPTxBuffer[43] = PVDFormat.BrakeStatus;

	UDPTxBuffer[44] = PVDFormat.SteeringAngle>>24;
	UDPTxBuffer[45] = PVDFormat.SteeringAngle>>16;
	UDPTxBuffer[46] = PVDFormat.SteeringAngle>>8;
	UDPTxBuffer[47] = PVDFormat.SteeringAngle;

	UDPTxBuffer[48] = PVDFormat.YawRate>>24;
	UDPTxBuffer[49] = PVDFormat.YawRate>>16;
	UDPTxBuffer[50] = PVDFormat.YawRate>>8;
	UDPTxBuffer[51] = PVDFormat.YawRate;

	UDPTxBuffer[52] = PVDFormat.ThrottlePosition>>24;
	UDPTxBuffer[53] = PVDFormat.ThrottlePosition>>16;
	UDPTxBuffer[54] = PVDFormat.ThrottlePosition>>8;
	UDPTxBuffer[55] = PVDFormat.ThrottlePosition;

	UDPTxBuffer[56] = PVDFormat.GNSSStatus>>24;
	UDPTxBuffer[57] = PVDFormat.GNSSStatus>>16;
	UDPTxBuffer[58] = PVDFormat.GNSSStatus>>8;
	UDPTxBuffer[59] = PVDFormat.GNSSStatus;

	UDPTxBuffer[60] = PVDFormat.DrivingMode>>24;
	UDPTxBuffer[61] = PVDFormat.DrivingMode>>16;
	UDPTxBuffer[62] = PVDFormat.DrivingMode>>8;
	UDPTxBuffer[63] = PVDFormat.DrivingMode;

	UDPTxBuffer[64] = PVDFormat.Horn>>24;
	UDPTxBuffer[65] = PVDFormat.Horn>>16;
	UDPTxBuffer[66] = PVDFormat.Horn>>8;
	UDPTxBuffer[67] = PVDFormat.Horn;

	UDPTxBuffer[68] = PVDFormat.Fallback>>24;
	UDPTxBuffer[69] = PVDFormat.Fallback>>16;
	UDPTxBuffer[70] = PVDFormat.Fallback>>8;
	UDPTxBuffer[71] = PVDFormat.Fallback;

	UDPTxBuffer[72] = PVDFormat.ValidationEventId>>24;
	UDPTxBuffer[73] = PVDFormat.ValidationEventId>>16;
	UDPTxBuffer[74] = PVDFormat.ValidationEventId>>8;
	UDPTxBuffer[75] = PVDFormat.ValidationEventId;

	UDPTxBuffer[76] = PVDFormat.ValidationEventValue>>24;
	UDPTxBuffer[77] = PVDFormat.ValidationEventValue>>16;
	UDPTxBuffer[78] = PVDFormat.ValidationEventValue>>8;
	UDPTxBuffer[79] = PVDFormat.ValidationEventValue;

	UDPTxBuffer[80] = PVDFormat.ValidationEventDetail>>56;
	UDPTxBuffer[81] = PVDFormat.ValidationEventDetail>>48;
	UDPTxBuffer[82] = PVDFormat.ValidationEventDetail>>40;
	UDPTxBuffer[83] = PVDFormat.ValidationEventDetail>>32;
	UDPTxBuffer[84] = PVDFormat.ValidationEventDetail>>24;
	UDPTxBuffer[85] = PVDFormat.ValidationEventDetail>>16;
	UDPTxBuffer[86] = PVDFormat.ValidationEventDetail>>8;
	UDPTxBuffer[87] = PVDFormat.ValidationEventDetail;

	UDPTxBuffer[88] = PVDFormat.SOC>>24;
	UDPTxBuffer[89] = PVDFormat.SOC>>16;
	UDPTxBuffer[90] = PVDFormat.SOC>>8;
	UDPTxBuffer[91] = PVDFormat.SOC;

	UDPTxBuffer[92] = PVDFormat.H2>>24;
	UDPTxBuffer[93] = PVDFormat.H2>>16;
	UDPTxBuffer[94] = PVDFormat.H2>>8;
	UDPTxBuffer[95] = PVDFormat.H2;

	UDPTxBuffer[96] = PVDFormat.Odometer>>24;
	UDPTxBuffer[97] = PVDFormat.Odometer>>16;
	UDPTxBuffer[98] = PVDFormat.Odometer>>8;
	UDPTxBuffer[99] = PVDFormat.Odometer;

	UDPTxBuffer[100] = PVDFormat.Tripmeter>>24;
	UDPTxBuffer[101] = PVDFormat.Tripmeter>>16;
	UDPTxBuffer[102] = PVDFormat.Tripmeter>>8;
	UDPTxBuffer[103] = PVDFormat.Tripmeter;

	UDPTxBuffer[104] = PVDFormat.distanceToEmpty>>24;
	UDPTxBuffer[105] = PVDFormat.distanceToEmpty>>16;
	UDPTxBuffer[106] = PVDFormat.distanceToEmpty>>8;
	UDPTxBuffer[107] = PVDFormat.distanceToEmpty;

	UDPTxBuffer[108] = PVDFormat.TurnSignal>>24;
	UDPTxBuffer[109] = PVDFormat.TurnSignal>>16;
	UDPTxBuffer[110] = PVDFormat.TurnSignal>>8;
	UDPTxBuffer[111] = PVDFormat.TurnSignal;

	UDPTxBuffer[112] = PVDFormat.PowerGauge>>24;
	UDPTxBuffer[113] = PVDFormat.PowerGauge>>16;
	UDPTxBuffer[114] = PVDFormat.PowerGauge>>8;
	UDPTxBuffer[115] = PVDFormat.PowerGauge;

	UDPTxBuffer[116] = PVDFormat.CurLane>>24;
	UDPTxBuffer[117] = PVDFormat.CurLane>>16;
	UDPTxBuffer[118] = PVDFormat.CurLane>>8;
	UDPTxBuffer[119] = PVDFormat.CurLane;

	UDPTxBuffer[120] = PVDFormat.Torque>>24;
	UDPTxBuffer[121] = PVDFormat.Torque>>16;
	UDPTxBuffer[122] = PVDFormat.Torque>>8;
	UDPTxBuffer[123] = PVDFormat.Torque;

	UDPTxBuffer[124] = PVDFormat.Acceleration>>24;
	UDPTxBuffer[125] = PVDFormat.Acceleration>>16;
	UDPTxBuffer[126] = PVDFormat.Acceleration>>8;
	UDPTxBuffer[127] = PVDFormat.Acceleration;

	UDPTxBuffer[128] = PVDFormat.DTC>>24;
	UDPTxBuffer[129] = PVDFormat.DTC>>16;
	UDPTxBuffer[130] = PVDFormat.DTC>>8;
	UDPTxBuffer[131] = PVDFormat.DTC;

	UDPTxBuffer[132] = PVDFormat.EngineTemp>>24;
	UDPTxBuffer[133] = PVDFormat.EngineTemp>>16;
	UDPTxBuffer[134] = PVDFormat.EngineTemp>>8;
	UDPTxBuffer[135] = PVDFormat.EngineTemp;

	UDPTxBuffer[136] = PVDFormat.CoolantTemp>>24;
	UDPTxBuffer[137] = PVDFormat.CoolantTemp>>16;
	UDPTxBuffer[138] = PVDFormat.CoolantTemp>>8;
	UDPTxBuffer[139] = PVDFormat.CoolantTemp;

	UDPTxBuffer[140] = PVDFormat.BatteryTemp>>24;
	UDPTxBuffer[141] = PVDFormat.BatteryTemp>>16;
	UDPTxBuffer[142] = PVDFormat.BatteryTemp>>8;
	UDPTxBuffer[143] = PVDFormat.BatteryTemp;

	UDPTxBuffer[144] = PVDFormat.BatteryWarning;
	UDPTxBuffer[145] = PVDFormat.TPMSWarning;
	UDPTxBuffer[146] = PVDFormat.ABSWarning;
	UDPTxBuffer[147] = PVDFormat.BrakeWarning;
}
