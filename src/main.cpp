/*
 * main implementation: use this 'C++' sample to create your own application
 *
 */
#include "S32V234.h"  /* include peripheral declarations */
#include "stdio.h"

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>

//thread
#include <unistd.h>
#include <pthread.h>
#include "LinuxCAN.h"
#include "LinuxEther.h"

//timer
#include <time.h>
#include <poll.h>
#include <errno.h>
#include <net/if.h>
//#include <linux/delay.h>

#include <sys/socket.h>
#include <linux/can.h>
#include <linux/can/raw.h>
//#include <linux/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/sockios.h>
#include <linux/net_tstamp.h>

#include "PTPHardwareClock.h"

#include "canlib.h"
#include "ether.h"
#include "DoIP_Header.h"

#hello
#define UDP_PORT  40001
#define DATA_BUF_LEN 30001
#define SourceIP      "192.168.1.111"
//#define SourceIP      "192.168.24.1"
int clientSocket = 0;
struct sockaddr_in clientSocket_addr, toServer_addr;
void DiagnosticsCheck(void);
void CANFDCheck(void);
//#define UDP_PORT  3285
//#define UDP_PORT  0xF74

#pragma pack(push, 1)
struct UDP_Frame{
int8_t UDP_field[1428];
};
#pragma pack(pop)

LinuxEther LinuxEth;
LinuxCAN LinuxCAN;

int diag_state=0;
uint8_t TransmitCnt10 = 0;
uint8_t TransmitCnt100 = 0;
//Extern for Diagnostics Variables
extern uint8_t AliveLiDAR_FL;
extern uint8_t AliveLiDAR_FR;
extern uint8_t AliveLiDAR_VFOV_F;
extern uint8_t AliveLiDAR_VFOV_L;
extern uint8_t AliveLiDAR_VFOV_R;
extern uint8_t AliveLiDAR_RL;
extern uint8_t AliveLiDAR_RR;
extern uint8_t AliveLPM;
extern uint8_t AliveGPSTime;
extern uint8_t AliveGPS;
extern uint8_t AliveV2XMAP;
extern uint8_t AliveV2XSPaT;
extern uint8_t AliveV2XTIM;
extern uint8_t AliveESU1;
extern uint8_t AliveESU2;
extern uint8_t AliveMRR_FrLf;
extern uint8_t AliveMRR_FrRg;
extern uint8_t AliveMRR_RrLf;
extern uint8_t AliveMRR_RrRg;
extern uint8_t AliveMRR_Rr;
extern uint8_t AliveKATECHController;
extern uint8_t AliveFrontCAM;
extern uint8_t AliveRearCAM;
extern uint8_t AliveSSVM;
extern uint8_t AliveHVI;
//Extern for Diagnostics Variables

uint8_t OldAliveLiDAR_FL = 0;
uint8_t OldAliveLiDAR_FR = 0;
uint8_t OldAliveLiDAR_VFOV_F = 0;
uint8_t OldAliveLiDAR_VFOV_L = 0;
uint8_t OldAliveLiDAR_VFOV_R = 0;
uint8_t OldAliveLiDAR_RL = 0;
uint8_t OldAliveLiDAR_RR = 0;
uint8_t OldAliveLPM = 0;
uint8_t OldAliveGPSTime = 0;
uint8_t OldAliveGPS = 0;
uint8_t OldAliveV2XMAP = 0;
uint8_t OldAliveV2XSPaT = 0;
uint8_t OldAliveV2XTIM = 0;
uint8_t OldAliveESU1 = 0;
uint8_t OldAliveESU2 = 0;
uint8_t OldAliveMRR_FrLf = 0;
uint8_t OldAliveMRR_FrRg = 0;
uint8_t OldAliveMRR_RrLf = 0;
uint8_t OldAliveMRR_RrRg = 0;
uint8_t OldAliveMRR_Rr = 0;
uint8_t OldAliveKATECHController = 0;
uint8_t OldAliveFrontCAM = 0;
uint8_t OldAliveRearCAM = 0;
uint8_t OldAliveSSVM = 0;
uint8_t OldAliveHVI = 0;

uint8_t AliveCheckKATECHController = 0;
uint8_t AliveCheckV2XTIM = 0;
uint8_t AliveCheckGPS = 0;
uint8_t AliveCheckV2XMAP = 0;
uint8_t AliveCheckESU1 = 0;
uint8_t AliveCheckESU2 = 0;
uint8_t AliveCheckMRR_FrLf = 0;
uint8_t AliveCheckMRR_FrRg = 0;
uint8_t AliveCheckMRR_RrLf = 0;
uint8_t AliveCheckMRR_RrRg = 0;
uint8_t AliveCheckMRR_Rr = 0;
uint8_t AliveCheckFrontCAM = 0;
uint8_t AliveCheckRearCAM = 0;
uint8_t AliveCheckSSVM = 0;
uint8_t AliveCheckHVI = 0;

uint8_t DiagnosticEdgeLogger = 0;
uint8_t DiagnosticADR = 0;
uint8_t DiagnosticPositioning = 0;
uint8_t DiagnosticMAP = 0;
uint8_t DiagnosticLiDAR_FL = 0;
uint8_t DiagnosticLiDAR_FR = 0;
uint8_t DiagnosticLiDAR_VFOV_F = 0;
uint8_t DiagnosticLiDAR_VFOV_L = 0;
uint8_t DiagnosticLiDAR_VFOV_R = 0;
uint8_t DiagnosticLiDAR_RL = 0;
uint8_t DiagnosticLiDAR_RR = 0;
uint8_t DiagnosticV2X = 0;
uint8_t DiagnosticLPM = 0;
uint8_t DiagnosticESU1 = 0;
uint8_t DiagnosticESU2 = 0;
uint8_t DiagnosticMRR_FrLf = 0;
uint8_t DiagnosticMRR_FrRg = 0;
uint8_t DiagnosticMRR_RrLf = 0;
uint8_t DiagnosticMRR_RrRg = 0;
uint8_t DiagnosticMRR_Rr = 0;
uint8_t DiagnosticKATECHController = 0;
uint8_t DiagnosticFrontCAM = 0;
uint8_t DiagnosticRearCAM = 0;
uint8_t DiagnosticSSVM = 0;
uint8_t DiagnosticHVI = 0;


// Ÿ�̸� �ֱ⿡ ���� ȣ��� Ÿ�̸�
int sockfd;

int cnt=0;

struct timespec Time1, Time2;
clockid_t PTPid;
long OldTime = 0;

//void timer()
void timer1(int a, siginfo_t* b, void*c)
{
	int32_t TimeGap;

	if (TransmitCnt10 == 10)
	{
		if (TransmitCnt100 == 10)
		{
			LinuxEth.UDPTxFunc();
 //           LinuxEth.UDPTxFunc_CANOE();
			TransmitCnt100 = 0;
		}
		TransmitCnt100++;

		DiagnosticsCheck();
		CANFDCheck();
		TransmitCnt10 = 0;

	}
	TransmitCnt10++;

	LinuxEth.AVTPTxFunc();

	/*
	clock_gettime(PTPid, &Time1);
//	clock_gettime(CLOCK_REALTIME, &spec2);
	printf("PTP_TimeStamp %ld.%09ld\n", Time1.tv_sec, Time1.tv_nsec);
	TimeGap = Time1.tv_nsec-OldTime;
	if(TimeGap<0) TimeGap+=1000000000;
	printf("PTP_TimeStamp GAP %09ld sec \n", TimeGap);
	OldTime = Time1.tv_nsec;
	*/
}

void timer2(int a, siginfo_t* b, void*c)
{


}

int createTimer1( timer_t *timerID, int sec, int msec )
{
    struct sigevent         te;
    struct itimerspec       its;
    struct sigaction        sa;
    int                     sigNo = SIGRTMIN;

    /* Set up signal handler. */
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = timer1;
    sigemptyset(&sa.sa_mask);

    if (sigaction(sigNo, &sa, NULL) == -1)
    {
        printf("sigaction error\n");
        return -1;
    }

    /* Set and enable alarm */
    te.sigev_notify = SIGEV_SIGNAL;
    te.sigev_signo = sigNo;
    te.sigev_value.sival_ptr = timerID;
    timer_create(CLOCK_REALTIME, &te, timerID);

    its.it_interval.tv_sec = sec;
    its.it_interval.tv_nsec = msec * 1000000;
    its.it_value.tv_sec = sec;

    its.it_value.tv_nsec = msec * 1000000;
    timer_settime(*timerID, 0, &its, NULL);

    return 0;
}

/*
int createTimer2( timer_t *timerID2, int sec, int msec )
{
    struct sigevent         te2;
    struct itimerspec       its2;
    struct sigaction        sa2;
    int                     sigNo2 = SIGRTMIN;

     Set up signal handler.
    sa2.sa_flags = SA_SIGINFO;
    sa2.sa_sigaction = timer2;
    sigemptyset(&sa2.sa_mask);

    if (sigaction(sigNo2, &sa2, NULL) == -1)
    {
        printf("sigaction error\n");
        return -1;
    }

     Set and enable alarm
    te2.sigev_notify = SIGEV_SIGNAL;
    te2.sigev_signo = sigNo2;
    te2.sigev_value.sival_ptr = timerID2;
    timer_create(CLOCK_REALTIME, &te2, timerID2);

    its2.it_interval.tv_sec = sec;
    its2.it_interval.tv_nsec = msec * 1000000;
    its2.it_value.tv_sec = sec;

    its2.it_value.tv_nsec = msec * 1000000;
    timer_settime(*timerID2, 0, &its2, NULL);

    return 0;
}
*/


int main()
{
   struct sockaddr_in servaddr;
   //pthread_t p_thread[2];
   //int thr_id;
   int status;
   int tid;

//   drpark test
   PTPid = phc_open("/dev/ptp0");
   LinuxCAN.InitCANChannel(0);
   LinuxCAN.ReceiveCANThread(0);

   LinuxCAN.InitCANFDChannel(1);
   LinuxCAN.ReceiveCANFDThread(1);

//   khjeong test
   LinuxCAN.InitCANChannel(2);
   LinuxCAN.ReceiveCANThread(2);
   LinuxCAN.InitCANChannel(3);
   LinuxCAN.ReceiveCANThread(3);


   LinuxEth.RxInit();
// LinuxEth.UDPTxInit_CANOE();
   LinuxEth.ReceiveThread();
   LinuxEth.AVTPTxInit();

//   struct UDP_Frame udp_frame;
//   std::thread thread1(can_rx);	//������ �� ���
// Ÿ�̸Ӹ� �����
    timer_t timerID;
/*    timer_t timerID2;*/
   /* ������ �ּҿ� ��Ʈ ��ȣ�� �̿��ؼ� �ּ� ���� */

    //sockfd = socket(AF_INET, SOCK_DGRAM, 0);                      /* UDP�� ���� ���� ���� */
    //bzero(&servaddr, sizeof(servaddr));
//    thread1.join();					//������� ���
    // �Ű����� 1 : Ÿ�̸� ����
    // �Ű����� 2 : second
    // �Ű����� 3 : ms

    createTimer1(&timerID,0, 10); 	//Rx

	 while(1){


	}
   return 0;
}

void DiagnosticsCheck(void)
{
	can_frame Cframe0;

	//  can test
	//	khjeong test
	Cframe0.can_id = 0x17F;
	Cframe0.can_dlc = 7;
	Cframe0.data[0] = rand()%8;
	Cframe0.data[1] = 0x31;
	Cframe0.data[2] = 0x32;
	Cframe0.data[3] = 0x33;
	Cframe0.data[4] = 0x34;
	Cframe0.data[5] = 0x35;
	Cframe0.data[6] = 0x36;
	LinuxCAN.WriteCANChannel(0, &Cframe0);
	Cframe0.data[1] = rand()%8;
	LinuxCAN.WriteCANChannel(2, &Cframe0);
	Cframe0.data[2] = rand()%8;
	LinuxCAN.WriteCANChannel(3, &Cframe0);

//	drpark
//	DiagnosticEdgeLogger = 0;
//	DiagnosticADR = 0;
//	/*
//	DiagnosticMAP = DiagnosticsLogic(AliveV2XMAP, OldAliveV2XMAP, AliveCheckV2XMAP, 50);
//	DiagnosticLiDAR_FL = 0;
//	DiagnosticLiDAR_FR = 0;
//	DiagnosticLiDAR_VFOV_F = 0;
//	DiagnosticLiDAR_VFOV_L = 0;
//	DiagnosticLiDAR_VFOV_R = 0;
//	DiagnosticLiDAR_RL = 0;
//	DiagnosticLiDAR_RR = 0;
//	DiagnosticLPM = 0;
//	DiagnosticRearCAM = DiagnosticsLogic(AliveRearCAM, OldAliveRearCAM, AliveCheckRearCAM, 5);
//*/
//
//	//Diagnostics for V2X Module
//	if (AliveV2XTIM == OldAliveV2XTIM)
//	{
//		AliveCheckV2XTIM++;
//		if (AliveCheckV2XTIM > 250)
//			AliveCheckV2XTIM = 250;
//	}
//	else
//	{
//		AliveCheckV2XTIM = 0;
//		OldAliveV2XTIM = AliveV2XTIM;
//	}
//
//	if (AliveCheckV2XTIM > 50)
//		DiagnosticV2X = 2;
//	else
//		DiagnosticV2X = 1;
//
//	//Diagnostics for GPS Module
//	if (AliveGPS == OldAliveGPS)
//	{
//		AliveCheckGPS++;
//		if (AliveCheckGPS > 250)
//			AliveCheckGPS = 250;
//	}
//	else
//	{
//		AliveCheckGPS = 0;
//		OldAliveGPS = AliveGPS;
//	}
//
//	if (AliveCheckGPS > 50)
//		DiagnosticPositioning = 2;
//	else
//		DiagnosticPositioning = 1;
//
//	//Diagnostics for ESU1
//	if (AliveESU1 == OldAliveESU1)
//	{
//		AliveCheckESU1++;
//		if (AliveCheckESU1 > 250)
//			AliveCheckESU1 = 250;
//	}
//	else
//	{
//		AliveCheckESU1 = 0;
//		OldAliveESU1 = AliveESU1;
//	}
//
//	if (AliveCheckESU1 > 50)
//		DiagnosticESU1 = 2;
//	else
//		DiagnosticESU1 = 1;
//
//	//Diagnostics for ESU2
//	if (AliveESU2 == OldAliveESU2)
//	{
//		AliveCheckESU2++;
//		if (AliveCheckESU2 > 250)
//			AliveCheckESU2 = 250;
//	}
//	else
//	{
//		AliveCheckESU2 = 0;
//		OldAliveESU2 = AliveESU2;
//	}
//
//	if (AliveCheckESU2 > 50)
//		DiagnosticESU2 = 2;
//	else
//		DiagnosticESU2 = 1;
//
//
//	//Diagnostics for AliveMRR_FrLf
//	if (AliveMRR_FrLf == OldAliveMRR_FrLf)
//	{
//		AliveCheckMRR_FrLf++;
//		if (AliveCheckMRR_FrLf > 250)
//			AliveCheckMRR_FrLf = 250;
//	}
//	else
//	{
//		AliveCheckMRR_FrLf = 0;
//		OldAliveMRR_FrLf = AliveMRR_FrLf;
//	}
//	if (AliveCheckMRR_FrLf > 5)
//		DiagnosticMRR_FrLf = 2;
//	else
//		DiagnosticMRR_FrLf = 1;
//
//	//Diagnostics for DiagnosticMRR_FrRg
//	if (AliveMRR_FrRg == OldAliveMRR_FrRg)
//	{
//		AliveCheckMRR_FrRg++;
//		if (AliveCheckMRR_FrRg > 250)
//			AliveCheckMRR_FrRg = 250;
//	}
//	else
//	{
//		AliveCheckMRR_FrRg = 0;
//		OldAliveMRR_FrRg = AliveMRR_FrRg;
//	}
//	if (AliveCheckMRR_FrRg > 5)
//		DiagnosticMRR_FrRg = 2;
//	else
//		DiagnosticMRR_FrRg = 1;
//
//	//Diagnostics for DiagnosticMRR_RrLf
//	if (AliveMRR_RrLf == OldAliveMRR_RrLf)
//	{
//		AliveCheckMRR_RrLf++;
//		if (AliveCheckMRR_RrLf > 250)
//			AliveCheckMRR_RrLf = 250;
//	}
//	else
//	{
//		AliveCheckMRR_RrLf = 0;
//		OldAliveMRR_RrLf = AliveMRR_RrLf;
//	}
//	if (AliveCheckMRR_RrLf > 5)
//		DiagnosticMRR_RrLf = 2;
//	else
//		DiagnosticMRR_RrLf = 1;
//
//	//Diagnostics for DiagnosticMRR_RrRg
//	if (AliveMRR_RrRg == OldAliveMRR_RrRg)
//	{
//		AliveCheckMRR_RrRg++;
//		if (AliveCheckMRR_RrRg > 250)
//			AliveCheckMRR_RrRg = 250;
//	}
//	else
//	{
//		AliveCheckMRR_RrRg = 0;
//		OldAliveMRR_RrRg = AliveMRR_RrRg;
//	}
//	if (AliveCheckMRR_RrRg > 5)
//		DiagnosticMRR_RrRg = 2;
//	else
//		DiagnosticMRR_RrRg = 1;
//
//	//Diagnostics for DiagnosticMRR_Rr
//	if (AliveMRR_Rr == OldAliveMRR_Rr)
//	{
//		AliveCheckMRR_Rr++;
//		if (AliveCheckMRR_Rr > 250)
//			AliveCheckMRR_Rr = 250;
//	}
//	else
//	{
//		AliveCheckMRR_Rr = 0;
//		OldAliveMRR_Rr = AliveMRR_Rr;
//	}
//	if (AliveCheckMRR_Rr > 5)
//		DiagnosticMRR_Rr = 2;
//	else
//		DiagnosticMRR_Rr = 1;
//
//	//Diagnostics for FrontCAM
//	if (AliveFrontCAM == OldAliveFrontCAM)
//	{
//		AliveCheckFrontCAM++;
//		if (AliveCheckFrontCAM > 250)
//			AliveCheckFrontCAM = 250;
//	}
//	else
//	{
//		AliveCheckFrontCAM = 0;
//		OldAliveFrontCAM = AliveFrontCAM;
//	}
//
//	if (AliveCheckFrontCAM > 5)
//		DiagnosticFrontCAM = 2;
//	else
//		DiagnosticFrontCAM = 1;
//
//	//Diagnostics for SSVM
//	if (AliveSSVM == OldAliveSSVM)
//	{
//		AliveCheckSSVM++;
//		if (AliveCheckSSVM > 250)
//			AliveCheckSSVM = 250;
//	}
//	else
//	{
//		AliveCheckSSVM = 0;
//		OldAliveSSVM = AliveSSVM;
//	}
//
//	if (AliveCheckSSVM > 5)
//		DiagnosticSSVM = 2;
//	else
//		DiagnosticSSVM = 1;
//
//	//Diagnostics for HVI
//	if (AliveHVI == OldAliveHVI)
//	{
//		AliveCheckHVI++;
//		if (AliveCheckHVI > 250)
//			AliveCheckHVI = 250;
//	}
//	else
//	{
//		AliveCheckHVI = 0;
//		OldAliveHVI = AliveHVI;
//	}
//	if (AliveCheckHVI > 5)
//		DiagnosticHVI = 2;
//	else
//		DiagnosticHVI = 1;
//
//	//Diagnostics for KATECH Controller
//	if (AliveKATECHController == OldAliveKATECHController)
//	{
//		AliveCheckKATECHController++;
//		if (AliveCheckKATECHController > 250)
//			AliveCheckKATECHController = 250;
//	}
//	else
//	{
//		AliveCheckKATECHController = 0;
//		OldAliveKATECHController = AliveKATECHController;
//	}
//	if (AliveCheckKATECHController > 5)
//		DiagnosticKATECHController = 2;
//	else
//		DiagnosticKATECHController = 1;

//	Cframe0.can_id = 0x17F;
//	Cframe0.can_dlc = 7;
//	Cframe0.data[0] = ((DiagnosticV2X&0x03)<<6) +  ((DiagnosticADR&0x03)<<4) + ((DiagnosticLPM&0x03)<<2) + (DiagnosticEdgeLogger&0x03);
//	Cframe0.data[1] = ((DiagnosticRearCAM&0x03)<<6) + ((DiagnosticFrontCAM&0x03)<<4) + ((DiagnosticMAP&0x03)<<2) + (DiagnosticPositioning&0x03);
//	Cframe0.data[2] = ((DiagnosticLiDAR_RR&0x03)<<6) + ((DiagnosticLiDAR_RL&0x03)<<4) + ((DiagnosticLiDAR_FR&0x03)<<2) + (DiagnosticLiDAR_FL&0x03);
//	Cframe0.data[3] = ((DiagnosticLiDAR_VFOV_R&0x03)<<4) + ((DiagnosticLiDAR_VFOV_L&0x03)<<2) + (DiagnosticLiDAR_VFOV_F&0x03);
//	Cframe0.data[4] = ((DiagnosticMRR_FrRg&0x03)<<6) + ((DiagnosticMRR_RrLf&0x03)<<4) + ((DiagnosticMRR_RrRg&0x03)<<2) + (DiagnosticMRR_Rr&0x03);
//	Cframe0.data[5] = ((DiagnosticSSVM&0x03)<<2) + (DiagnosticMRR_FrLf&0x03);
//	Cframe0.data[6] = ((DiagnosticKATECHController&0x03)<<6) + ((DiagnosticHVI&0x03)<<4) + ((DiagnosticESU2&0x03)<<2) + (DiagnosticESU1&0x03);
//	LinuxCAN.WriteCANChannel(0, &Cframe0);



}

void CANFDCheck(void)
{
	//	khjeong test

	//can fd test
	canfd_frame CFDframe1;

	CFDframe1.can_id = 0x112;
	CFDframe1.len = 16;
	CFDframe1.flags = 0;
	CFDframe1.__res0 = 0;
	CFDframe1.__res1 = 0;
	CFDframe1.data[0] = rand()%8;
	CFDframe1.data[1] = 0x30;
	CFDframe1.data[2] = 0x31;
	CFDframe1.data[3] = 0x32;
	CFDframe1.data[4] = 0x33;
	CFDframe1.data[5] = 0x34;
	CFDframe1.data[6] = 0x35;
	CFDframe1.data[7] = 0x36;
	LinuxCAN.WriteCANFDChannel(1, &CFDframe1);

}
