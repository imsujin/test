/*
 * rxcan.cpp
 *
 *  Created on: 2019. 2. 15.
 *      Author: Mario
 */
#include <unistd.h>
#include <pthread.h>
#include "LinuxCAN.h"

INSPVACMP_frame inspvacmp_frame;
uint32_t Latitude;
uint32_t Longitude;
uint16_t  Azimuth;
double WGS84_180 = 1800000000;//180x10^7
double split = 10000000;
double degree=549755813888;//2^39
uint64_t Latitude1;
uint64_t Longitude1;


//Extern variables for LinuxEther.cpp
uint8_t GearPosition = 0;
uint8_t AcceleratorAct = 0;
uint32_t CurrentVehicleLatitude = 0;
uint32_t CurrentVehicleLongitude = 0;
float VS = 0;
uint8_t HeadLightStatus = 0;
uint8_t RightTurnStatus = 0;
uint8_t LeftTurnStatus = 0;
uint8_t ACCAct = 0;
float AccelPedPos = 0;
float Fb_Brake_Press1 = 0;
float CurrentSteeringAngle = 45;
float StrTorque = 0;
float LongAccel = 0;
float LatAccel = 0;
float YawRate = 0;
//Extern variables for LinuxEther.cpp

uint8_t AliveMRR_FrLf = 0;
uint8_t AliveMRR_FrRg = 0;
uint8_t AliveMRR_RrLf = 0;
uint8_t AliveMRR_RrRg = 0;
uint8_t AliveMRR_Rr = 0;
uint8_t AliveKATECHController = 0;
uint8_t AliveFrontCAM = 0;
uint8_t AliveRearCAM = 0;
uint8_t AliveSSVM = 0;
uint8_t AliveHVI = 0;


uint8_t PVDTxDataFrame[200] = {0x01, 0x03, 0x05, 0x0F, 0x13, 0xA3, 0x00, 0x0F};

void PVDFormatGeneration(void);
void CRC32Compute(void);



LinuxCAN::LinuxCAN()
{
    for(int i=0; i<CAN_CH_MAX; i++)
    {
        can_sock[i]=-1;

    }
}

LinuxCAN::~LinuxCAN()
{
    for(int i=0; i<CAN_CH_MAX; i++)
    {
        if(can_sock[i]<0)
        {

        }
        else
        {
            close(can_sock[i]);
            pthread_cancel(CanReceiveThread[i]);
            pthread_cancel(CanFDReceiveThread[i]);
            printf("CAN%d Channel Closed.\n",i);
        }
    }
    
}

int LinuxCAN::InitCANChannel(int ch)
{
    char data8[32] = {0};
    int thr_id;
    int enable_canfd = 1;
    struct ifreq        ifr;
    struct sockaddr_can addr;

    struct timeval timeout = {1, 0};

    /* open fd */
    this->can_sock[ch] = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (this->can_sock[ch] < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] socket : errno=%d\n", e);
        return e;
    }

    addr.can_family = AF_CAN;
    sprintf(data8, "can%d", ch);
    strcpy(ifr.ifr_name, data8); //khjeong can can0
//    strcpy(ifr.ifr_name, "can0");

    if (ioctl(this->can_sock[ch], SIOCGIFINDEX, &ifr) < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] ioctl : errno=%d\n", e);
        return e;
    }

    addr.can_ifindex = ifr.ifr_ifindex;

    fcntl(this->can_sock[ch], F_SETFL, O_NONBLOCK);

    if (bind(this->can_sock[ch], (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] bind : errno=%d\n", e);
        return e;
    }

    FD_ZERO(&this->fdset[ch]);
    FD_SET(this->can_sock[ch], &this->fdset[ch]);
    if (select((this->can_sock[ch] + 1), &this->fdset[ch], NULL, NULL, &timeout) < 0)
    return -1;

    printf("CAN %d Channel Opened.\n",ch);
    return 0;
 }

int LinuxCAN::InitCANFDChannel(int ch)
{
    char data8[32] = {0}; 
    int thr_id;
    int enable_canfd = 1;
    struct ifreq        ifr;
    struct sockaddr_can addr;

    struct timeval timeout = {1, 0};
    
    /* open fd */
    this->can_sock[ch] = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (this->can_sock[ch] < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] socket : errno=%d\n", e);
        return e;
    }

    addr.can_family = AF_CAN;
    sprintf(data8, "can%d", ch);
    strcpy(ifr.ifr_name, data8);
//    strcpy(ifr.ifr_name, "can1"); //khjeong CANFD can1

    if (ioctl(this->can_sock[ch], SIOCGIFINDEX, &ifr) < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] ioctl : errno=%d\n", e);
        return e;
    }

    addr.can_ifindex = ifr.ifr_ifindex;

    fcntl(this->can_sock[ch], F_SETFL, O_NONBLOCK);

    if (setsockopt(this->can_sock[ch], SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
			   &enable_canfd, sizeof(enable_canfd))){
		printf("error when enabling CAN FD support\n");
		return 1;
	}

    if (bind(this->can_sock[ch], (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        int e = errno;
        fprintf(stderr, "[can_open] bind : errno=%d\n", e);
        return e;
    }

    FD_ZERO(&this->fdset[ch]);
    FD_SET(this->can_sock[ch], &this->fdset[ch]);
    if (select((this->can_sock[ch] + 1), &this->fdset[ch], NULL, NULL, &timeout) < 0)
    return -1; 

    printf("CAN FD %d Channel Opened.\n",ch);
    return 0;
 }

void LinuxCAN::ReceiveCANThread(int ch)
{
	int thr_id;
	this->tmp = ch;
	thr_id = pthread_create(&this->CanReceiveThread[ch], NULL, CANReceiveFunc, (void *)this);
}

void LinuxCAN::ReceiveCANFDThread(int ch)
{
	int thr_id;
	this->tmp2 = ch;
	thr_id = pthread_create(&this->CanFDReceiveThread[ch], NULL, CANFDReceiveFunc, (void *)this);
}


int LinuxCAN::ReadCANChannel(int ch, struct can_frame *cframe)
{
	int ret;

	if(this->can_sock[ch] > 0)
    {
		if (FD_ISSET(this->can_sock[ch], &this->fdset[ch]))
		{
			ret = read(this->can_sock[ch], cframe, sizeof(struct can_frame));

			if(ret==-1) ret = -1;
			else ret = 0;



			if (cframe->can_id == 0x21)   //KATECHCommand
			{
				printf("AliveKATECHController\n");
				AliveKATECHController++;
				if (AliveKATECHController == 16)
					AliveKATECHController = 0;
			}
			if (cframe->can_id == 0x200)   //CPM_F
			{
				printf("AliveFrontCAM\n");
				AliveFrontCAM++;
				if (AliveFrontCAM == 16)
					AliveFrontCAM = 0;
			}

			if (cframe->can_id == 0x174)
			{
				LongAccel = ((cframe->data[0]+(cframe->data[1]<<8))*0.0001274)-4.175;
				YawRate = ((cframe->data[2]+(cframe->data[3]<<8))*0.005)-163.84;
			}


			if (cframe->can_id == 0x41)
			{
				CurrentSteeringAngle = (cframe->data[0]+(cframe->data[1]<<8))*0.1;
				if (CurrentSteeringAngle > 3276.7)
					CurrentSteeringAngle = CurrentSteeringAngle - 6553.5;
				StrTorque = (cframe->data[3]+((cframe->data[4]&0x0F)<<8))*0.01-20.48;
			}


			if (cframe->can_id == 0x52)
			{
				VS = (cframe->data[0]+((cframe->data[1]&0x0F)<<8))*0.0625;
				HeadLightStatus = ((cframe->data[2])&0x08)>>3;
				RightTurnStatus = ((cframe->data[2])&0x04)>>2;
				LeftTurnStatus = ((cframe->data[2])&0x02)>>1;
			}
			if (cframe->can_id == 0x40)
			{
				AccelPedPos = (cframe->data[0]*0.4);
				GearPosition = (cframe->data[1]&0xF0)>>4;
			}
			if (cframe->can_id == 0x32)
			{
				Fb_Brake_Press1 = (cframe->data[3]+((cframe->data[4]&0x0F)<<8))*0.01;

			}
			if (cframe->can_id == 0x33)
			{
				ACCAct = cframe->data[0]&0x01;

			}
			if (cframe->can_id == 0x340)   //Object#1 from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x341)   //Object#2 from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x342)   //Object#3 from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x343)   //Object#4 from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x344)   //Object#5 from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x345)   //StopLine from SSVM
			{
				AliveSSVM++;
				if (AliveSSVM == 16)
					AliveSSVM = 0;
			}
			if (cframe->can_id == 0x370)   //DriverStatus from HVI
			{
				AliveHVI++;
				if (AliveHVI == 16)
					AliveHVI = 0;
			}
			if (cframe->can_id == 0x371)   //HoDData from HVI
			{
				AliveHVI++;
				if (AliveHVI == 16)
					AliveHVI = 0;
			}

			//GPS Position
			if (cframe->can_id == 0x100)
			{
				CurrentVehicleLatitude = cframe->data[0];
				CurrentVehicleLongitude = cframe->data[1];
			}

			//EMS AcceleratorPedal
			if (cframe->can_id == 0x4F1)
			{
				AcceleratorAct = cframe->data[0];
			}

		}
		else ret = -1;
    }
    else ret = -1;

    return ret;
}

int LinuxCAN::ReadCANFDChannel(int ch, struct canfd_frame *cframe)
{
	int ret;

	if(this->can_sock[ch] > 0)
    {
		if (FD_ISSET(this->can_sock[ch], &this->fdset[ch]))
		{
			ret = read(this->can_sock[ch], cframe, sizeof(struct canfd_frame));

			if(ret==-1) ret = -1;
			else ret = 0;

			if (cframe->can_id == 0x610)
			{
				AliveMRR_FrLf++;
				if (AliveMRR_FrLf == 16)
					AliveMRR_FrLf = 0;
			}
			if (cframe->can_id == 0x640)
			{
				AliveMRR_FrRg++;
				if (AliveMRR_FrRg == 16)
					AliveMRR_FrRg = 0;
			}
			if (cframe->can_id == 0x670)
			{
				AliveMRR_RrLf++;
				if (AliveMRR_RrLf == 16)
					AliveMRR_RrLf = 0;
			}
			if (cframe->can_id == 0x6A0)
			{
				AliveMRR_RrRg++;
				if (AliveMRR_RrRg == 16)
					AliveMRR_RrRg = 0;
			}
			if (cframe->can_id == 0x6D0)
			{
				AliveMRR_Rr++;
				if (AliveMRR_Rr == 16)
					AliveMRR_Rr = 0;
			}

		}
		else ret = -1;
    }
    else ret = -1;

    return ret;
}

int LinuxCAN::WriteCANChannel(int ch, struct can_frame *cframe)
{
	write(this->can_sock[ch], cframe, sizeof(struct can_frame));
	return 0;
}

int LinuxCAN::WriteCANFDChannel(int ch, struct canfd_frame *cframe)
{

	//canfd can1
	//drpark
//	canfd_frame CFDframe0;


//	CFDframe0.can_id = 0x112;
//	CFDframe0.len = 16;
//	CFDframe0.flags = 0;
//	CFDframe0.__res0 = 0;
//	CFDframe0.__res1 = 0;
//	CFDframe0.data[0] = 0;
//	WriteCANFDChannel(1, &CFDframe0);

	write(this->can_sock[ch], cframe, sizeof(struct canfd_frame));
	return 0;
}


void *CANReceiveFunc(void *lcan)
{
  int ret;
  int ch;
  can_frame cf;
  LinuxCAN *llcan = (LinuxCAN *)lcan;
  ch=llcan->tmp;
  while(1)
  {
	  if(llcan->ReadCANChannel(0,&cf)==0)
	  {

	  }
  }
}

void *CANFDReceiveFunc(void *lcan)
{
  int ret;
  int ch;
  canfd_frame cf;
  LinuxCAN *llcan = (LinuxCAN *)lcan;
  ch=llcan->tmp2;
  while(1)
  {
	  if(llcan->ReadCANFDChannel(1,&cf)==0)
	  {

	  }
  }
}

