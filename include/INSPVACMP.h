/*
 * INSPVACMP.h
 *
 *  Created on: 2019. 7. 16.
 *      Author: Mario
 */

#ifndef INSPVACMP_H_
#define INSPVACMP_H_


typedef struct frame{  //uint8_t UDP_field[48]; //�� 48����Ʈ ���
	uint8_t Length;
	uint32_t MessageTime;
	uint8_t INSStatus;
	uint8_t GNSSPositionType;
	uint8_t Latitude_WGS84[5];
	uint8_t Longitude_WGS84[5];
	uint32_t Height;
	uint16_t NorthVelo;
	uint16_t EastVelo;
	uint16_t UpVelo;
	uint16_t Roll;
	uint16_t Pitch;
	uint16_t Azimuth;
	uint16_t AzimuthRate;
	uint8_t test[8];
} INSPVACMP_frame;



#endif /* INSPVACMP_H_ */
