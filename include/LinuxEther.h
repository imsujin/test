/*
 * UDP.h
 *
 *  Created on: 2020. 3. 02.
 *      Author: Mario
 */ 

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//#include "Frametest.h"

#ifndef UDP_H_
#define UDP_H_


class LinuxEther
{
	private: 
	

//     pthread_t UDPTransmitThread;
     	pthread_t UDPV2XTIMRecvThread;
     	pthread_t UDPV2XSPaTRecvThread;
     	pthread_t UDPMAPRecvThread;
     	pthread_t UDPGPSRecvThread;
     	pthread_t UDPGPSTimeRecvThread;
     	pthread_t UDPLPMRecvThread;
		pthread_t UDPCANOERecvThread;
		pthread_t AVTPRecvThread;
		//udp Tx
 		int tx, sockfd_AICM	;
		int sockfd_CANOE;
 		//int thr_id_tx;
		
		//UDP Rx
		int UDPRxV2XTIM, uUDPRxV2XTIM, UDPRxV2XSPaT, uUDPRxV2XSPaT, UDPRxMAP, uUDPRxMAP, UDPRxGPS, uUDPRxGPS, UDPRxGPSTime, uUDPRxGPSTime, UDPRxLPM, uUDPRxLPM, UDPRxCANOE, uUDPRxCANOE;
		int AVTPRx, uAVTPRx;
		int ThrID_UDPV2XTIM_Rx;
		int ThrID_UDPV2XSPaT_Rx;
		int ThrID_UDPMAP_Rx;
		int ThrID_UDPGPS_Rx;
		int ThrID_UDPGPSTime_Rx;
		int ThrID_UDPLPM_Rx;
		int ThrID_UDPCANOE_Rx;
		int ThrID_UDPCANOE1_Rx;
		int ThrID_AVTP_Rx;
	
	protected:

		//udp Tx

		int enabled =1;
		struct sockaddr_in tx_addr,AICM_addr;
		struct sockaddr_in CANOE_addr; 
		
//   	static void *udp_tx_func(void *temp);
	
		//udp Rx
		struct sockaddr_in rx_addr, client_addr;
		char UDPRecvBuffer[1500];
		char UDPTransBuffer[500];
		char AVTPRecvBuffer[1500];
		socklen_t rx_addr_len;
		static void *UDPV2XTIMRxFunc(void *temp);
		static void *UDPV2XSPaTRxFunc(void *temp);
		static void *UDPMAPRxFunc(void *temp);
		static void *UDPGPSRxFunc(void *temp);
		static void *UDPGPSTimeRxFunc(void *temp);
		static void *UDPLPMRxFunc(void *temp);
		static void *AVTPRxFunc(void *temp);
		static void *UDPCANOERxFunc(void *temp);
		//void *udp_rx_func(void *udprx);

        
	public:
		LinuxEther();
		~LinuxEther();
		//udp Tx	
		int tx_init();
		int AVTPTxInit(void);
		int UDPTxFunc(void);
		int UDPTxInit_CANOE(void);
		int UDPTxFunc_CANOE(void);
		int AVTPTxFunc(void);
//		void *udp_tx_func(void);
//		void run_tx_thread(void);
//		int send_AICM = 0;
		
		//udp Rx
		int RxInit();
		void ReceiveThread(void);
	
};



#endif /*  UDP_H_ */

