/*
 * Frametest.h
 *
 *  Created on: 2020. 3. 02.
 *      Author: Mario
 */ 

#ifndef FRAMETEST_H_
#define FRAMETEST_H_

typedef struct test_frame{  //
	uint32_t test1;
	uint32_t test2;
	uint16_t test3; 
	uint16_t test4;

}__attribute__((packed)) Test_Frame; //




#endif /* FRAMETEST_H_ */

