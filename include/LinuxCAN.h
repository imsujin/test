/*
 * LinuxCAN.h
 *
 *  Created on: 2019. 10. 10.
 *      Author: Mario
 */

#ifndef LINUXCAN_H_
#define LINUXCAN_H_



#include <stdio.h>
#include <string.h>

#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <errno.h>


#define CAN_CH_MAX 2
#define can_buff_size 32
#define canFD_buff_size 64

class LinuxCAN
{
 private:
    //int can_sock[CAN_CH_MAX];
    pthread_t CanReceiveThread[CAN_CH_MAX];
    pthread_t CanFDReceiveThread[CAN_CH_MAX];
        
  public:
		LinuxCAN();
		~LinuxCAN();
    int flag[CAN_CH_MAX]; 
    fd_set fdset[CAN_CH_MAX];
    int overwrite[CAN_CH_MAX];
    can_frame rxframe[CAN_CH_MAX][can_buff_size];
    canfd_frame FDrxframe[CAN_CH_MAX][canFD_buff_size];
    //canfd_frame FDrxframe[CAN_CH_MAX][can_buff_size];
	int tmp;
	int tmp2;
    int can_sock[CAN_CH_MAX];
    int InitCANChannel(int ch);
    int InitCANFDChannel(int ch);
    int ReadCANChannel(int ch, struct can_frame *cframe);
    int ReadCANFDChannel(int ch, struct canfd_frame *cframe);
//    int write_ch(int ch, struct can_frame *cframe);
    int WriteCANChannel(int ch, struct can_frame *cframe);
    int WriteCANFDChannel(int ch, struct canfd_frame *cframe);
    void ReceiveCANThread(int ch);
    void ReceiveCANFDThread(int ch);
};

void *CANReceiveFunc(void *lcan);
void *CANFDReceiveFunc(void *lcan);

typedef struct _sINSPVACMPframe{  //uint8_t UDP_field[48]; //
	uint8_t Length;
	uint32_t MessageTime;
	uint8_t INSStatus;
	uint8_t GNSSPositionType;
	uint8_t Latitude_WGS84[5];
	uint8_t Longitude_WGS84[5];
	uint32_t Height;
	uint16_t NorthVelo;
	uint16_t EastVelo;
	uint16_t UpVelo;
	uint16_t Roll;
	uint16_t Pitch;
	uint16_t Azimuth;
	uint16_t AzimuthRate;
	uint8_t test[8];
} INSPVACMP_frame;

#endif /* LINUXCAN_H_ */
